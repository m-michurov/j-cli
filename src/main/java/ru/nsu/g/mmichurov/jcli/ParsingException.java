package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;

public class ParsingException extends Exception {

    public ParsingException(final @NotNull String message) {
        super(message);
    }
}
