package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Possible types of arguments.
 * <p>
 * Inheritors describe type of argument value. New types can be added by user.
 * In case of options type can have parameter or not.
 */
public abstract class ArgType<T> {
    final boolean hasParameter;

    public ArgType(final boolean hasParameter) {
        this.hasParameter = hasParameter;
    }

    /**
     * Text description of type for helpMessage.
     */
    public abstract @NotNull String getDescription();

    /**
     * Function to convert string argument value to its type.
     * In case of error during conversion also provides help message.
     *
     * @param value value
     */
    public abstract @NotNull T convert(final @NotNull String value, @NotNull String optionName) throws ParsingException;

    /**
     * Argument type for flags that can be only set/unset.
     */
    public static final ArgType<java.lang.Boolean> Boolean = new ArgType<>(false) {

        @Override
        public  @NotNull String getDescription() {
            return "";
        }

        @Override
        public @NotNull java.lang.Boolean convert(@NotNull final String value, @NotNull final String optionName) {
            return !"false".equals(value);
        }
    };

    /**
     * Argument type for string values.
     */
    public static final ArgType<java.lang.String> String = new ArgType<>(true) {

        @Override
        public @NotNull java.lang.String getDescription() {
            return "{ String }";
        }

        @Override
        public @NotNull java.lang.String convert(final java.lang.@NotNull String value, @NotNull final String optionName) {
            return value;
        }
    };

    /**
     * Argument type for integer values.
     */
    public static final ArgType<java.lang.Integer> Integer = new ArgType<>(true) {

        @Override
        public @NotNull java.lang.String getDescription() {
            return "{ Integer }";
        }

        @Override
        public @NotNull java.lang.Integer convert(
                @NotNull final String value,
                @NotNull final String optionName) throws ParsingException {
            try {
                return java.lang.Integer.parseInt(value);
            } catch (final NumberFormatException e) {
                throw new ParsingException(java.lang.String.format(
                        "Option %s is expected to be integer number. %s is provided.", optionName, value
                ));
            }
        }
    };

    /**
     * Argument type for double values.
     */
    public static final ArgType<java.lang.Double> Double = new ArgType<>(true) {

        @Override
        public @NotNull java.lang.String getDescription() {
            return "{ Double }";
        }

        @Override
        public @NotNull java.lang.Double convert(
                @NotNull final String value,
                @NotNull final String optionName) throws ParsingException {
            try {
                return java.lang.Double.parseDouble(value);
            } catch (final NumberFormatException e) {
                throw new ParsingException(java.lang.String.format(
                        "Option %s is expected to be double number. %s is provided.", optionName, value
                ));
            }
        }
    };

    /**
     * Create a type for arguments that have limited set of possible values represented as enumeration constants.
     */
    public static <EnumType extends Enum<EnumType>> ArgType<EnumType> choiceOf(final Class<EnumType> enumTypeClass)
            throws IllegalArgumentException {
        return new Choice<>(EnumSet.allOf(enumTypeClass));
    }

    /**
     * Create a type for arguments that have limited set of possible values.
     */
    public static <T> ArgType<T> choiceOf(final Collection<T> choices) throws IllegalArgumentException {
        return new Choice<>(choices);
    }

    /**
     * Create a type for arguments that have limited set of possible values.
     */
    @SafeVarargs
    public static <T> ArgType<T> choiceOf(final T... choices) throws IllegalArgumentException {
        return new Choice<>(choices);
    }

    /**
     * Type for arguments that have limited set of possible values.
     */
    private static final class Choice<V> extends ArgType<V> {
        private final @NotNull List<@NotNull String> choicesStringValues;
        private final @NotNull Map<java.lang.String, V> choicesMap = new HashMap<>();

        private Choice(final Collection<V> choices) throws IllegalArgumentException {
            super(true);
            this.fillChoicesMap(choices);
            this.choicesStringValues = choices.stream()
                    .map(it -> it.toString().toLowerCase())
                    .sorted()
                    .collect(Collectors.toList());
        }

        @SafeVarargs
        private Choice(final V... choices) throws IllegalStateException {
            this(Arrays.asList(choices));
        }

        private void fillChoicesMap(final Collection<? extends V> choices) throws IllegalArgumentException {
            choices.forEach((v) -> this.choicesMap.put(v.toString().toLowerCase(), v));
            if (this.choicesMap.keySet().size() != choices.size() || choices.isEmpty()) {
                throw new IllegalArgumentException("Command line representations of enum choices are not distinct");
            }
        }

        @Override
        public @NotNull java.lang.String getDescription() {
            return java.lang.String.format("{ Value should be one of %s }", this.choicesStringValues);
        }

        @Override
        public @NotNull V convert(@NotNull final String value, @NotNull final String optionName) throws ParsingException {
            final @Nullable var choice = this.choicesMap.get(value);
            if (choice != null) {
                return choice;
            }
            throw new ParsingException(java.lang.String.format(
                    "Option %s is expected to be one of %s. %s is provided.",
                    optionName, this.choicesStringValues, value
            ));
        }
    }
}
