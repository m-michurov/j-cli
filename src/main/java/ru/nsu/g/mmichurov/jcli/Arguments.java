package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Arguments {

    /**
     * The base class for command line arguments.
     *
     * You can use [ArgParser.argument] function to declare an argument.
     */
    abstract static class Argument<T, TResult> extends CLIEntity<T, TResult> {

        protected Argument(
                @NotNull final ArgumentValues.ParsingValue<T, TResult> delegate,
                @NotNull final CLIEntityWrapper owner) {
            super(delegate, owner);
        }
    }

    /**
     * The base class of an argument with a single value.
     */
    abstract static class AbstractSingleArgument<T>
            extends Argument<T, T> {

        protected AbstractSingleArgument(
                @NotNull final ArgumentValues.ParsingValue<T, T> delegate,
                @NotNull final CLIEntityWrapper owner) {
            super(delegate, owner);
        }

        /**
         * Check descriptor for this kind of argument.
         */
        protected void checkDescriptor(@NotNull final Descriptors.ArgumentDescriptor<?, ?> descriptor)
                throws IllegalArgumentException {
            if (descriptor.num == null || descriptor.num > 1) {
                throw new IllegalArgumentException(
                        "Argument with single value can't be initialized with descriptor for multiple values."
                );
            }
        }

        private @NotNull Descriptors.ArgumentDescriptor<T, List<T>> createDescriptorForMultipleValues(
                @Nullable final Integer num) {
            return new Descriptors.ArgumentDescriptor<>(
                    this.delegate.descriptor.type,
                    this.delegate.descriptor.fullName,
                    num,
                    this.delegate.descriptor.description,
                    Collections.emptyList(),
                    this.delegate.descriptor.required,
                    this.delegate.descriptor.deprecatedWarning
            );
        }

        protected @NotNull MultipleRequiredArgument<T> toMultipleArgument(@Nullable final Integer num) {
            final @NotNull var newArgument = new MultipleRequiredArgument<>(
                    this.createDescriptorForMultipleValues(num),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        protected @NotNull MultipleMaybeEmptyArgument<T> toMultipleMaybeEmptyArgument(@Nullable final Integer num) {
            final @NotNull var newArgument = new MultipleMaybeEmptyArgument<>(
                    this.createDescriptorForMultipleValues(num),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }
    }

    /**
     * A non-optional argument.
     *
     * The [value] of such argument is non-null.
     */
    public static final class SingleRequiredArgument<T>
            extends AbstractSingleArgument<@NotNull T> {
        private final Descriptors.@NotNull ArgumentDescriptor<T, T> descriptor;

        SingleRequiredArgument(
                final Descriptors.@NotNull ArgumentDescriptor<T, T> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentSingleValue<>(descriptor), owner);
            this.checkDescriptor(descriptor);
            this.descriptor = descriptor;
        }

        @Override
        public @NotNull T getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        /**
         * Allows the argument to have no value specified in command line string.
         *
         * The value of the argument is `null` in case if no value was specified in command line string.
         *
         * Note that only trailing arguments can be optional, i.e. no required arguments can follow optional ones.
         */
        public @NotNull SingleNullableArgument<T> optional() {
            final @NotNull var newArgument = new SingleNullableArgument<>(
                    new Descriptors.ArgumentDescriptor<>(
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.num,
                            this.descriptor.description,
                            this.descriptor.defaultValue,
                            false,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        /**
         * Specifies the default value for the argument, that will be used when no value is provided for the argument
         * in command line string.
         *
         * Argument becomes optional, because value for it is set even if it isn't provided in command line.
         *
         * @param value the default value.
         */
        public @NotNull SingleArgumentWithDefault<T> setDefault(@NotNull final T value) {
            return this.optional().setDefault(value);
        }

        /**
         * Allows the argument to have several values specified in command line string.
         *
         * @param num the exact number of values expected for this argument, but at least 2.
         *
         * @throws IllegalArgumentException if number of values expected for this argument less than 2.
         */
        public @NotNull MultipleRequiredArgument<T> multiple(final int num) throws IllegalArgumentException {
            if (num < 2) {
                throw new IllegalArgumentException(
                        "multiple() modifier with value less than 2 is unavailable. It's already set to 1."
                );
            }

            return this.toMultipleArgument(num);
        }

        /**
         * Allows the last argument to take all the trailing values in command line string.
         */
        public @NotNull MultipleRequiredArgument<T> vararg() {
            return this.toMultipleArgument(null);
        }
    }

    /**
     * An optional argument with nullable [value].
     */
    public static final class SingleNullableArgument<T>
            extends AbstractSingleArgument<@Nullable T> {
        private final @NotNull Descriptors.ArgumentDescriptor<T, T> descriptor;

        SingleNullableArgument(
                @NotNull final Descriptors.ArgumentDescriptor<T, T> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentSingleNullableValue<>(descriptor), owner);
            this.checkDescriptor(descriptor);
            this.descriptor = descriptor;
        }

        /**
         * Specifies the default value for the argument, that will be used when no value is provided for the argument
         * in command line string.
         *
         * @param value the default value.
         */
        public @NotNull SingleArgumentWithDefault<T> setDefault(@NotNull final T value) {
            final @NotNull var newArgument = new SingleArgumentWithDefault<>(
                    new Descriptors.ArgumentDescriptor<>(
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.num,
                            this.descriptor.description,
                            value,
                            this.descriptor.required,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        /**
         * Allows the argument to have several values specified in command line string.
         *
         * @param num the exact number of values expected for this argument, but at least 2.
         *
         * @throws IllegalArgumentException if number of values expected for this argument less than 2.
         */
        public @NotNull MultipleMaybeEmptyArgument<T> multiple(final int num) throws IllegalArgumentException {
            if (num < 2) {
                throw new IllegalArgumentException(
                        "multiple() modifier with value less than 2 is unavailable. It's already set to 1."
                );
            }

            return this.toMultipleMaybeEmptyArgument(num);
        }

        /**
         * Allows the last argument to take all the trailing values in command line string.
         */
        public @NotNull MultipleMaybeEmptyArgument<T> vararg() {
            return this.toMultipleMaybeEmptyArgument(null);
        }
    }

    /**
     * An optional argument with a default value.
     */
    public static final class SingleArgumentWithDefault<T>
            extends AbstractSingleArgument<@NotNull T> {
        SingleArgumentWithDefault(
                @NotNull final Descriptors.ArgumentDescriptor<T, T> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentSingleValue<>(descriptor), owner);
            this.checkDescriptor(descriptor);
        }

        @Override
        protected void checkDescriptor(@NotNull final Descriptors.ArgumentDescriptor<?, ?> descriptor)
                throws IllegalArgumentException {
            super.checkDescriptor(descriptor);
            if (!descriptor.defaultValueSet) {
                throw new IllegalArgumentException(
                        "Argument with default value can't be initialized using descriptor without default value."
                );
            }
        }

        @Override
        public @NotNull T getValue() {
            return Objects.requireNonNull(super.getValue());
        }
    }

    /**
     * An argument that allows several values to be provided in command line string.
     *
     * The [value] property of such argument has type `List<T>`.
     */
    public static final class MultipleRequiredArgument<T> extends Argument<T, List<T>> {
        private final @NotNull Descriptors.ArgumentDescriptor<T, List<T>> descriptor;

        MultipleRequiredArgument(
                @NotNull final Descriptors.ArgumentDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (descriptor.num != null && descriptor.num < 2) {
                throw new IllegalArgumentException(
                        "Argument with multiple values can't be initialized with descriptor for single one."
                );
            }

            this.descriptor = descriptor;
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        /**
         * Allows the argument with multiple values to have no values specified in command line string.
         *
         * The value of the argument is an empty list in case if no value was specified in command line string.
         *
         * Note that only trailing arguments can be optional: no required arguments can follow the optional ones.
         */
        public @NotNull MultipleMaybeEmptyArgument<T> optional() {
            final @NotNull var newArgument = new MultipleMaybeEmptyArgument<>(
                    new Descriptors.ArgumentDescriptor<>(
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.num,
                            this.descriptor.description,
                            Collections.emptyList(),
                            false,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        /**
         * Specifies the default value for the argument with multiple values, that will be used when no values are provided
         * for the argument in command line string.
         *
         * Argument becomes optional, because value for it is set even if it isn't provided in command line.
         *
         * @param value the default value, must be a non-empty collection.
         */
        public @NotNull MultipleArgumentWithDefault<T> setDefault(final @NotNull Collection<T> value) {
            return this.optional().setDefault(value);
        }
    }

    public static final class MultipleMaybeEmptyArgument<T> extends Argument<T, List<T>> {
        private final @NotNull Descriptors.ArgumentDescriptor<T, List<T>> descriptor;

        MultipleMaybeEmptyArgument(
                @NotNull final Descriptors.ArgumentDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (descriptor.num != null && descriptor.num < 2) {
                throw new IllegalArgumentException(
                        "Argument with multiple values can't be initialized with descriptor for single one."
                );
            }

            this.descriptor = descriptor;
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        private @NotNull MultipleArgumentWithDefault<T> toMultipleArgumentWithDefault(
                final @NotNull Collection<? extends T> value) {
            final @NotNull var newArgument = new MultipleArgumentWithDefault<>(
                    new Descriptors.ArgumentDescriptor<>(
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.num,
                            this.delegate.descriptor.description,
                            new ArrayList<>(value),
                            this.descriptor.required,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        /**
         * Specifies the default value for the argument with multiple values, that will be used when no values are provided
         * for the argument in command line string.
         *
         * @param value the default value, must be a non-empty collection.
         */
        public @NotNull MultipleArgumentWithDefault<T> setDefault(final @NotNull Collection<T> value) {
            if (value.isEmpty()) {
                throw new IllegalArgumentException("Default value for argument can't be empty collection.");
            }

            assert this.descriptor.num != null;
            if (value.size() != this.descriptor.num) {
                throw new IllegalArgumentException(
                        String.format(
                                "Size of the default collection (%d) does not match argument number (%d).",
                                value.size(), this.descriptor.num
                        )
                );
            }

            return this.toMultipleArgumentWithDefault(value);
        }
    }

    public static final class MultipleArgumentWithDefault<T> extends Argument<T, List<T>> {

        MultipleArgumentWithDefault(
                @NotNull final Descriptors.ArgumentDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (descriptor.num != null && descriptor.num < 2) {
                throw new IllegalArgumentException(
                        "Argument with multiple values can't be initialized with descriptor for single one."
                );
            }
            if (!descriptor.defaultValueSet) {
                throw new IllegalArgumentException(
                        "Argument with default value can't be initialized with descriptor without default value."
                );
            }
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }
    }
}
