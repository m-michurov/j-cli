package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;

/**
 * Argument parsing result.
 * Contains name of subcommand which was called.
 */
public final class ParsingResult {
    final @NotNull String commandName;

    /**
     * @param commandName name of command which was called.
     */
    ParsingResult(@NotNull final String commandName) {
        this.commandName = commandName;
    }
}
