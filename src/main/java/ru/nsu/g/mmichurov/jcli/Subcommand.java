package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;

/**
 * Abstract base class for subcommands.
 */
public abstract class Subcommand extends ArgumentParser {
    final @NotNull String name;
    final @NotNull String actionDescription;

    public Subcommand(@NotNull final String name, @NotNull final String actionDescription) {
        super(name);
        this.name = name;
        this.actionDescription = actionDescription;
    }

    /**
     * Execute action if subcommand was provided.
     */
    public abstract void execute();

    @NotNull String getHelpMessage() {
        return String.format("    %s - %s\n", this.programName, this.actionDescription);
    }
}
