package ru.nsu.g.mmichurov.jcli;

import ru.nsu.g.mmichurov.jcli.ArgumentValues.ParsingValue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Queue of arguments descriptors.
 * Arguments can have several values, so one descriptor can be returned several times.
 */
final class ArgumentsQueue {

    /**
     * Map of arguments descriptors and their current usage number.
     */
    private final @NotNull Map<Descriptors.@NotNull ArgumentDescriptor<?, ?>, @NotNull Integer> argumentsUsageNumber;

    ArgumentsQueue(@NotNull final Iterable<Descriptors.@NotNull ArgumentDescriptor<?, ?>> argumentsDescriptors) {
        this.argumentsUsageNumber = new LinkedHashMap<>();
        argumentsDescriptors.forEach((it) -> this.argumentsUsageNumber.put(it, 0));
    }

    /**
     * Get next descriptor from queue.
     */
    @Nullable String pop() {
        if (this.argumentsUsageNumber.isEmpty()) {
            return null;
        }

        final var next =
                this.argumentsUsageNumber.entrySet().iterator().next();

        final @NotNull var currentDescriptor = next.getKey();
        final int usageNumber = next.getValue();

        if (currentDescriptor.num != null) {
            // Parse all arguments for current argument description.
            if (usageNumber + 1 >= currentDescriptor.num) {
                // All needed arguments were provided.
                this.argumentsUsageNumber.remove(currentDescriptor);
            } else {
                this.argumentsUsageNumber.put(currentDescriptor, usageNumber + 1);
            }
        }

        return currentDescriptor.fullName;
    }
}

interface ValueHolder<T> {
    @Nullable T getValue();

    @NotNull ArgumentParser.ValueOrigin getValueOrigin();
}

/**
 * Arguments parser.
 */
public class ArgumentParser {
    final @NotNull String programName;
    boolean useDefaultHelpShortName;
    @NotNull OptionPrefixStyle prefixStyle;
    boolean skipExtraArguments;

    /**
     * Map of options: key - full name of option, value - pair of descriptor and parsed values.
     */
    private final @NotNull HashMap<@NotNull String, @NotNull ParsingValue<?, ?>> options = new HashMap<>();

    /**
     * Map of arguments: key - full name of argument, value - pair of descriptor and parsed values.
     */
    private final @NotNull HashMap<@NotNull String, @NotNull ParsingValue<?, ?>> arguments = new LinkedHashMap<>();

    /**
     * Map with declared options.
     */
    private final @NotNull ArrayList<@NotNull CLIEntityWrapper> declaredOptions = new ArrayList<>();

    /**
     * Map with declared arguments.
     */
    private final @NotNull ArrayList<@NotNull CLIEntityWrapper> declaredArguments = new ArrayList<>();

    /**
     * State of parser. Stores last parsing result or null.
     */
    private @Nullable ParsingResult parsingState = null;

    /**
     * Map of subcommands.
     */
    protected @NotNull HashMap<@NotNull String, @NotNull Subcommand> subcommands = new HashMap<>();

    /**
     * Mapping for short options names for quick search.
     */
    private final Map<@NotNull String, @NotNull ParsingValue<?, ?>> shortNames = new HashMap<>();

    /**
     * Used prefix form for full option form.
     */
    private final @NotNull String optionFullFormPrefix;

    /**
     * Used prefix form for short option form.
     */
    private final @NotNull String optionShortFromPrefix = "-";

    /**
     * Name with all commands that should be executed.
     */
    protected final List<@NotNull String> fullCommandName;

    /**
     * Flag to recognize if CLI entities can be treated as options.
     */
    protected boolean treatAsOption = true;

    /**
     * Arguments which should be parsed with subcommands.
     */
    private final List<@NotNull String> subcommandsArguments = new ArrayList<>();

    /**
     * Options  which should be parsed with subcommands.
     */
    private final @NotNull ArrayList<@NotNull String> subcommandsOptions = new ArrayList<>();

    /**
     * Subcommand used in command line arguments.
     */
    private @Nullable Subcommand usedSubcommand = null;


    /**
     * Created parser will use default help short name and Linux option prefix style, and also won't skip extra
     * arguments.
     *
     * @param programName the name of the current program.
     */
    public ArgumentParser(@NotNull final String programName) {
        this(programName, true, OptionPrefixStyle.LINUX, false);
    }

    /**
     * Created parser will use default help short name and Linux option prefix style.
     *
     * @param programName        the name of the current program.
     * @param skipExtraArguments specifies whether the extra unmatched arguments in a command line string
     *                           can be skipped without producing an error message.
     */
    public ArgumentParser(
            @NotNull final String programName,
            final boolean skipExtraArguments) {
        this(programName, true, OptionPrefixStyle.LINUX, skipExtraArguments);
    }

    /**
     * Created parser will use default help short name and won't skip extra arguments.
     *
     * @param programName the name of the current program.
     * @param prefixStyle the style of option prefixing.
     */
    public ArgumentParser(
            @NotNull final String programName,
            @NotNull final OptionPrefixStyle prefixStyle) {
        this(programName, true, prefixStyle, false);
    }

    /**
     * @param programName             the name of the current program.
     * @param useDefaultHelpShortName specifies whether to register "-h" option for printing the usage information.
     * @param prefixStyle             the style of option prefixing.
     * @param skipExtraArguments      specifies whether the extra unmatched arguments in a command line string
     *                                can be skipped without producing an error message.
     */
    public ArgumentParser(
            @NotNull final String programName,
            final boolean useDefaultHelpShortName,
            @NotNull final OptionPrefixStyle prefixStyle,
            final boolean skipExtraArguments) {
        this.programName = programName;
        this.useDefaultHelpShortName = useDefaultHelpShortName;
        this.prefixStyle = prefixStyle;
        this.skipExtraArguments = skipExtraArguments;

        this.optionFullFormPrefix = this.prefixStyle == OptionPrefixStyle.JVM ? "-" : "--";
        this.fullCommandName = new ArrayList<>(Collections.singletonList(this.programName));
    }

    /**
     * Declares a named flag and returns an object which can be used to access the flag value
     * after all arguments are parsed.
     * <p>
     * By default, the flag supports only a single value, is optional, and has a default value `false`,
     * therefore its value's type is `@NotNull Boolean`.
     * <p>
     * You can alter the option properties by chaining extensions for the option type on the returned object.
     * - [delimiter] to allow specifying multiple values in one command line argument with a delimiter;
     * - [multiple] to allow specifying the option several times.
     *
     * @param fullName          the full name of the flag.
     * @param shortName         the short name of the flag, `null` if the flag cannot be specified in a short form.
     * @param description       the description of the flag used when rendering the usage information.
     * @param deprecatedWarning the deprecation message for the flag.
     *                          Specifying anything except `null` makes this flag deprecated. The message is rendered in a help message and
     *                          issued as a warning when the flag is encountered when parsing command line arguments.
     */
    public @NotNull Options.SingleOptionWithDefault<Boolean> flag(
            @NotNull final String fullName,
            @Nullable final String shortName,
            @Nullable final String description,
            @Nullable final String deprecatedWarning) {
        return this.option(ArgType.Boolean, fullName, shortName, description, deprecatedWarning).setDefault(false);
    }

    /**
     * Declares a named flag and returns an object which can be used to access the flag value
     * after all arguments are parsed.
     * <p>
     * By default, the flag supports only a single value, is optional, and has a default value `false`,
     * therefore its value's type is `@NotNull Boolean`.
     * <p>
     * You can alter the option properties by chaining extensions for the option type on the returned object.
     * - [delimiter] to allow specifying multiple values in one command line argument with a delimiter;
     * - [multiple] to allow specifying the option several times.
     *
     * @param fullName    the full name of the flag.
     * @param shortName   the short name of the flag, `null` if the flag cannot be specified in a short form.
     * @param description the description of the flag used when rendering the usage information.
     */
    public @NotNull Options.SingleOptionWithDefault<Boolean> flag(
            @NotNull final String fullName,
            @Nullable final String shortName,
            @Nullable final String description) {
        return this.flag(fullName, shortName, description, null);
    }

    /**
     * Declares a named option and returns an object which can be used to access the option value
     * after all arguments are parsed.
     * <p>
     * By default, the option supports only a single value, is optional, and has no default value,
     * therefore its value's type is `@Nullable T`.
     * <p>
     * You can alter the option properties by chaining extensions for the option type on the returned object.
     * - [setDefault] to provide a default value that is used when the option is not specified;
     * - [required] to make the option non-optional;
     * - [delimiter] to allow specifying multiple values in one command line argument with a delimiter;
     * - [multiple] to allow specifying the option several times.
     *
     * @param type              The type describing how to parse an option value from a string,
     *                          an instance of [ArgType], e.g. [ArgType.String].
     *                          Use [ArgumentParser.flag] to declare an option with type [ArgType.Boolean] and
     *                          a default value `false`.
     * @param fullName          the full name of the option.
     * @param shortName         the short name of the option, `null` if the option cannot be specified in a short form.
     * @param description       the description of the option used when rendering the usage information.
     * @param deprecatedWarning the deprecation message for the option.
     *                          Specifying anything except `null` makes this option deprecated. The message is rendered in a help message and
     *                          issued as a warning when the option is encountered when parsing command line arguments.
     */
    public <T> Options.@NotNull SingleNullableOption<T> option(
            @NotNull final ArgType<T> type,
            @NotNull final String fullName,
            @Nullable final String shortName,
            @Nullable final String description,
            @Nullable final String deprecatedWarning) {
        if (this.prefixStyle == OptionPrefixStyle.GNU && shortName != null) {
            if (shortName.length() != 1) {
                throw new IllegalArgumentException(
                        "GNU standard for options allow to use short form which consists of one character.\n" +
                                "For more information, please, see " +
                                "https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html"
                );
            }
        }
        final var option = new Options.SingleNullableOption<>(
                new Descriptors.OptionDescriptor<>(
                        this.optionFullFormPrefix,
                        this.optionShortFromPrefix,
                        type,
                        fullName,
                        shortName,
                        description,
                        null,
                        false,
                        false,
                        null,
                        deprecatedWarning
                ),
                new CLIEntityWrapper()
        );
        this.declaredOptions.add(option.owner);
        return option;
    }

    /**
     * Declares a named option and returns an object which can be used to access the option value
     * after all arguments are parsed.
     * <p>
     * By default, the option supports only a single value, is optional, and has no default value,
     * therefore its value's type is `@Nullable T`.
     * <p>
     * You can alter the option properties by chaining extensions for the option type on the returned object.
     * - [setDefault] to provide a default value that is used when the option is not specified;
     * - [required] to make the option non-optional;
     * - [delimiter] to allow specifying multiple values in one command line argument with a delimiter;
     * - [multiple] to allow specifying the option several times.
     *
     * @param type        The type describing how to parse an option value from a string,
     *                    an instance of [ArgType], e.g. [ArgType.String].
     *                    Use [ArgumentParser.flag] to declare an option with type [ArgType.Boolean] and
     *                    a default value `false`.
     * @param fullName    the full name of the option.
     * @param shortName   the short name of the option, `null` if the option cannot be specified in a short form.
     * @param description the description of the option used when rendering the usage information.
     */
    public <T> Options.@NotNull SingleNullableOption<T> option(
            @NotNull final ArgType<T> type,
            @NotNull final String fullName,
            @Nullable final String shortName,
            @Nullable final String description) {
        return this.option(type, fullName, shortName, description, null);
    }

    /**
     * Check usage of required property for arguments.
     * Make sense only for several last arguments.
     */
    private void inspectRequiredAndDefaultUsage() throws IllegalStateException {
        @Nullable ParsingValue<?, ?> previousArgument = null;

        for (final @NotNull var entry : this.arguments.entrySet()) {
            final @NotNull var currentArgument = entry.getValue();

            if (previousArgument != null) {
                if (previousArgument.descriptor.defaultValueSet) {
                    if (!currentArgument.descriptor.defaultValueSet && currentArgument.descriptor.required) {
                        throw new IllegalStateException(
                                String.format(
                                        "Default value of argument %s will be unused,  " +
                                                "because next argument %s is always required and " +
                                                "has no default value.",
                                        previousArgument.descriptor.fullName,
                                        currentArgument.descriptor.fullName)
                        );
                    }
                }
                // Previous argument is optional.
                if (!previousArgument.descriptor.required) {
                    if (!currentArgument.descriptor.defaultValueSet && currentArgument.descriptor.required) {
                        throw new IllegalStateException(
                                String.format(
                                        "Argument %s will be always required, because " +
                                                "next argument %s is always required.",
                                        previousArgument.descriptor.fullName,
                                        currentArgument.descriptor.fullName)
                        );
                    }
                }
            }

            previousArgument = currentArgument;
        }
    }

    /**
     * Declares an argument and returns an object which can be used to access the argument value
     * after all arguments are parsed.
     * <p>
     * By default, the argument supports only a single value, is required, and has no default value,
     * therefore its value's type is `@NotNull T`.
     * <p>
     * You can alter the argument properties by chaining extensions for the argument type on the returned object:
     * - [setDefault] to provide a default value that is used when the argument is not specified;
     * - [optional] to allow omitting the argument;
     * - [multiple] to require the argument to have exactly the number of values specified;
     * - [vararg] to allow specifying an unlimited number of values for the _last_ argument.
     *
     * @param type              The type describing how to parse an option value from a string,
     *                          an instance of [ArgType], e.g. [ArgType.String].
     * @param fullName          the full name of the argument.
     * @param description       the description of the argument used when rendering the usage information.
     * @param deprecatedWarning the deprecation message for the argument.
     *                          Specifying anything except `null` makes this argument deprecated. The message is rendered in a help message and
     *                          issued as a warning when the argument is encountered when parsing command line arguments.
     */
    public <T> Arguments.@NotNull SingleRequiredArgument<T> argument(
            @NotNull final ArgType<T> type,
            @NotNull final String fullName,
            @Nullable final String description,
            @Nullable final String deprecatedWarning) {
        final @NotNull var argument = new Arguments.SingleRequiredArgument<>(
                new Descriptors.ArgumentDescriptor<>(
                        type,
                        fullName,
                        1,
                        description,
                        null,
                        true,
                        deprecatedWarning
                ),
                new CLIEntityWrapper()
        );

        argument.owner.entity = argument;
        this.declaredArguments.add(argument.owner);
        return argument;
    }

    /**
     * Declares an argument and returns an object which can be used to access the argument value
     * after all arguments are parsed.
     * <p>
     * By default, the argument supports only a single value, is required, and has no default value,
     * therefore its value's type is `@NotNull T`.
     * <p>
     * You can alter the argument properties by chaining extensions for the argument type on the returned object:
     * - [setDefault] to provide a default value that is used when the argument is not specified;
     * - [optional] to allow omitting the argument;
     * - [multiple] to require the argument to have exactly the number of values specified;
     * - [vararg] to allow specifying an unlimited number of values for the _last_ argument.
     *
     * @param type        The type describing how to parse an option value from a string,
     *                    an instance of [ArgType], e.g. [ArgType.String].
     * @param fullName    the full name of the argument.
     * @param description the description of the argument used when rendering the usage information.
     */
    public <T> Arguments.@NotNull SingleRequiredArgument<T> argument(
            @NotNull final ArgType<T> type,
            @NotNull final String fullName,
            @Nullable final String description) {
        return this.argument(type, fullName, description, null);
    }

    /**
     * Registers one or more subcommands.
     *
     * @param subcommandsList subcommands to add.
     */
    public void subcommands(@NotNull final Subcommand... subcommandsList) {
        for (final @NotNull var it : subcommandsList) {
            if (this.subcommands.containsKey(it.name)) {
                throw new IllegalStateException(
                        String.format("Subcommand with name %s was already defined.", it.name)
                );
            }

            // Set same settings as main parser.
            it.prefixStyle = this.prefixStyle;
            it.useDefaultHelpShortName = this.useDefaultHelpShortName;

            for (int i = 0; i < this.fullCommandName.size(); i++) {
                it.fullCommandName.add(i, this.fullCommandName.get(i));
            }

            this.subcommands.put(it.name, it);
        }
    }

    /**
     * Outputs an error message adding the usage information after it.
     *
     * @param message error message.
     */
    private void printError(@NotNull final String message) throws IllegalStateException {
        throw new IllegalStateException(
                String.format("%s\n%s", message, this.makeUsage())
        );
    }

    private void addHelpOption() {
        final @NotNull var helpDescriptor = new Descriptors.OptionDescriptor<Boolean, Boolean>(
                this.optionFullFormPrefix,
                this.optionShortFromPrefix,
                ArgType.Boolean,
                "help",
                this.useDefaultHelpShortName ? "h" : null,
                "Usage info",
                null,
                false,
                false,
                null,
                null
        );

        final @NotNull var helpOption = new Options.SingleNullableOption<>(helpDescriptor, new CLIEntityWrapper());
        this.declaredOptions.add(helpOption.owner);
    }

    /**
     * Save value as argument value.
     *
     * @param arg            string with argument value.
     * @param argumentsQueue queue with active argument descriptors.
     */
    private boolean saveAsArg(@NotNull final String arg, @NotNull final ArgumentsQueue argumentsQueue)
            throws ParsingException {
        // Find next uninitialized arguments.
        final var name = argumentsQueue.pop();

        if (name != null) {
            final @NotNull var argumentValue = Objects.requireNonNull(this.arguments.get(name));

            // [addValue] will output a deprecated warning anyway
            //            if (argumentValue.descriptor.deprecatedWarning != null) {
            //                printWarning(argumentValue.descriptor.deprecatedWarning);
            //            }

            argumentValue.addValue(arg);
            return true;
        }

        return false;
    }

    /**
     * Treat value as argument value.
     *
     * @param arg            string with argument value.
     * @param argumentsQueue queue with active argument descriptors.
     */
    private void treatAsArgument(@NotNull final String arg, @NotNull final ArgumentsQueue argumentsQueue)
            throws ParsingException {
        if (!this.saveAsArg(arg, argumentsQueue)) {
            if (this.usedSubcommand != null) {
                (this.treatAsOption ? this.subcommandsOptions : this.subcommandsArguments).add(arg);
            } else {
                this.printError(String.format("Too many arguments! Couldn't process argument %s!", arg));
            }
        }
    }

    /**
     * Save value as option value.
     */
    private <T, U> void saveAsOption(@NotNull final ParsingValue<T, U> parsingValue, @NotNull final String value)
            throws ParsingException {
        parsingValue.addValue(value);
    }

    /**
     * Try to recognize and save command line element as full form of option.
     *
     * @param candidate   string with candidate in options.
     * @param argIterator iterator over command line arguments.
     */
    private boolean recognizeAndSaveOptionFullForm(
            @NotNull final String candidate, @NotNull final Iterator<String> argIterator) throws
            ParsingException {
        if (this.prefixStyle == OptionPrefixStyle.GNU && candidate.equals(this.optionFullFormPrefix)) {
            // All other arguments after `--` are treated as non-option arguments.
            this.treatAsOption = false;
            return false;
        }
        if (!candidate.startsWith(this.optionFullFormPrefix)) {
            return false;
        }

        final var optionString = candidate.substring(this.optionFullFormPrefix.length());
        final var argValue =
                this.prefixStyle == OptionPrefixStyle.GNU ? null : this.options.get(optionString);

        if (argValue != null) {
            this.saveStandardOptionForm(argValue, argIterator);
            return true;
        } else {
            // Check GNU style of options.
            if (this.prefixStyle == OptionPrefixStyle.GNU) {
                // Option without a parameter.
                final var parsingValue = this.options.get(optionString);
                if (parsingValue != null && !parsingValue.descriptor.type.hasParameter) {
                    this.saveOptionWithoutParameter(this.options.get(optionString));
                    return true;
                }
                // Option with parameters.
                final var optionParts = optionString.split("=", 2);
                if (optionParts.length != 2) {
                    return false;
                }
                if (this.options.get(optionParts[0]) != null) {
                    this.saveAsOption(
                            Objects.requireNonNull(this.options.get(optionParts[0])),
                            optionParts[1]
                    );
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Save option without parameter.
     *
     * @param argValue argument value with all information about option.
     */
    protected void saveOptionWithoutParameter(@NotNull final ParsingValue<?, ?> argValue) throws ParsingException {
        // Boolean flags.
        if ("help".equals(argValue.descriptor.fullName)) {
            if (this.usedSubcommand != null) {
                this.usedSubcommand.saveOptionWithoutParameter(argValue);
            }
            if (this.declaredOptions.stream().noneMatch(it -> {
                assert it.entity != null;
                return "help".equals(it.entity.delegate.descriptor.fullName);
            })) {
                this.addHelpOption();
            }
            System.out.println((this.makeUsage()));
            System.exit(0);
        }
        this.saveAsOption(argValue, "true");
    }

    /**
     * Save option described with standard separated form `--name value`.
     *
     * @param argValue    argument value with all information about option.
     * @param argIterator iterator over command line arguments.
     */
    private void saveStandardOptionForm(
            @NotNull final ParsingValue<?, ?> argValue, @NotNull final Iterator<String> argIterator) throws
            ParsingException {
        if (argValue.descriptor.type.hasParameter) {
            if (argIterator.hasNext()) {
                this.saveAsOption(argValue, argIterator.next());
            } else {
                // An error, option with value without value.
                this.printError("No value for ${argValue.descriptor.textDescription}");
            }
        } else {
            this.saveOptionWithoutParameter(argValue);
        }
    }

    /**
     * Try to recognize and save command line element as short form of option.
     *
     * @param candidate   string with candidate in options.
     * @param argIterator iterator over command line arguments.
     */
    private boolean recognizeAndSaveOptionShortForm(
            @NotNull final String candidate, @NotNull final Iterator<String> argIterator) throws
            ParsingException {
        if (!candidate.startsWith(this.optionShortFromPrefix)
                || !this.optionFullFormPrefix.equals(this.optionShortFromPrefix)
                && candidate.startsWith(this.optionFullFormPrefix)) {
            return false;
        }
        // Try to find exact match.
        final var option = candidate.substring(this.optionShortFromPrefix.length());
        final var argValue = this.shortNames.get(option);
        if (argValue != null) {
            this.saveStandardOptionForm(argValue, argIterator);
        } else {
            if (this.prefixStyle != OptionPrefixStyle.GNU || option.isEmpty()) {
                return false;
            }

            // Try to find collapsed form.
            final @Nullable var firstOption = this.shortNames.get(String.valueOf(option.charAt(0)));
            if (firstOption == null) {
                return false;
            }
            // Form with value after short form without separator.
            if (firstOption.descriptor.type.hasParameter) {
                this.saveAsOption(firstOption, option.substring(1));
            } else {
                // Form with several short forms as one string.
                final var otherBooleanOptions = option.substring(1);
                this.saveOptionWithoutParameter(firstOption);

                for (final var opt : otherBooleanOptions.toCharArray()) {
                    final var it = this.shortNames.get(String.valueOf(opt));
                    if (it != null) {
                        if (it.descriptor.type.hasParameter) {
                            this.printError(
                                    String.format(
                                            "Option %s%c can't be used in option combination %s, " +
                                                    "because parameter value of type %s should be " +
                                                    "provided for current option.",
                                            this.optionShortFromPrefix, opt, candidate,
                                            it.descriptor.type.getDescription())
                            );
                        }
                    } else {
                        this.printError(String.format(
                                "Unknown option %s%c in option combination $candidate.",
                                this.optionShortFromPrefix, opt
                        ));
                    }

                    this.saveOptionWithoutParameter(it);
                }
            }
        }
        return true;
    }

    /**
     * Parses the provided array of command line arguments.
     * After a successful parsing, the options and arguments declared in this parser get their values.
     *
     * @param args the array with command line arguments.
     * @return an [ArgParserResult] if all arguments were parsed successfully.
     * Otherwise, prints the usage information and terminates the program execution.
     * @throws IllegalStateException in case of attempt of calling parsing several times.
     */
    public @NotNull ParsingResult parse(@NotNull final String... args) throws IllegalStateException {
        return this.parse(Arrays.asList(args));
    }

    protected @NotNull ParsingResult parse(@NotNull final List<@NotNull String> args) throws IllegalStateException {
        if (this.parsingState != null) {
            throw new IllegalStateException("Parsing of command line options can be called only once.");
        }

        this.addHelpOption();

        // Add default list with arguments if there can be extra free arguments.
        if (this.skipExtraArguments) {
            this.argument(ArgType.String, "", null, null).vararg();
        }

        // Clean options and arguments maps.
        this.options.clear();
        this.arguments.clear();

        // Map declared options and arguments to maps.
        this.declaredOptions.forEach(option -> {
            final var value = Objects.requireNonNull(option.entity).delegate;

            // Add option.
            final @NotNull var it = value.descriptor.fullName;
            if (this.options.containsKey(it)) {
                throw new IllegalStateException(
                        String.format("Option with full name %s was already added.", it));
            }

            final @NotNull var descriptor = (Descriptors.OptionDescriptor<?, ?>) value.descriptor;
            if (descriptor.shortName != null && this.shortNames.containsKey(descriptor.shortName)) {
                throw new IllegalStateException(
                        String.format("Option with short name %s was already added.", descriptor.shortName));
            }

            if (descriptor.shortName != null) {
                this.shortNames.put(descriptor.shortName, value);
            }

            this.options.put(it, value);
        });

        this.declaredArguments.forEach(argument -> {
            final var value = Objects.requireNonNull(argument.entity).delegate;

            final @NotNull var it = value.descriptor.fullName;
            // Add option.
            if (this.arguments.containsKey(it)) {
                throw new IllegalStateException(
                        String.format("Argument with full name %s was already added.", it));
            }
            this.arguments.put(it, value);
        });
        // Make inspections for arguments.
        this.inspectRequiredAndDefaultUsage();

        Arrays.asList(this.arguments, this.options).forEach(
                it -> it.forEach(
                        (unused, value) -> value.valueOrigin = ValueOrigin.UNSET
                )
        );

        final var descriptorsList = new LinkedList<Descriptors.ArgumentDescriptor<?, ?>>();
        this.arguments.forEach((unused, value1) -> descriptorsList.add((Descriptors.ArgumentDescriptor<?, ?>) value1.descriptor));
        final @NotNull var argumentsQueue = new ArgumentsQueue(descriptorsList);

        this.usedSubcommand = null;
        this.subcommandsOptions.clear();
        this.subcommandsArguments.clear();

        final @NotNull var argIterator = args.listIterator();

        try {
            while (argIterator.hasNext()) {
                final var arg = argIterator.next();

                // Check for subcommands.
                if (this.subcommands.containsKey(arg)) {
                    this.usedSubcommand = this.subcommands.get(arg);
                } else {
                    // Parse arguments from command line.
                    if (this.treatAsOption && arg.startsWith("-")) {
                        // Candidate in being option.
                        // Option is found.
                        if (!(this.recognizeAndSaveOptionShortForm(arg, argIterator)
                                || this.recognizeAndSaveOptionFullForm(arg, argIterator))) {
                            // State is changed so next options are arguments.
                            if (this.treatAsOption) {
                                if (this.usedSubcommand != null) {
                                    this.subcommandsOptions.add(arg);
                                } else {
                                    // Try save as argument.
                                    if (!this.saveAsArg(arg, argumentsQueue)) {
                                        this.printError(
                                                String.format("Unknown option %s", arg));
                                    }
                                }
                            } else {
                                // Argument is found.
                                this.treatAsArgument(argIterator.next(), argumentsQueue);
                            }
                        }
                    } else {
                        // Argument is found.
                        this.treatAsArgument(arg, argumentsQueue);
                    }
                }
            }
            // Postprocess results of parsing.
            final var toBeProcessed = new ArrayList<ParsingValue<?, ?>>();
            toBeProcessed.addAll(this.options.values());
            toBeProcessed.addAll(this.arguments.values());
            toBeProcessed.forEach(value -> {
                // Not inited, append default value if needed.
                if (value.isEmpty()) {
                    value.addDefaultValue();
                }
                if (value.valueOrigin != ValueOrigin.SET_BY_USER && value.descriptor.required) {
                    this.printError(
                            String.format(
                                    "Value for %s should be always provided in command line.",
                                    value.descriptor.getTextDescription()
                            )
                    );
                }
            });

            if (this.usedSubcommand != null) {
                final var it = this.usedSubcommand;
                final var subcommandArgumentsList = new ArrayList<>(this.subcommandsOptions);
                if (!this.treatAsOption) {
                    subcommandArgumentsList.add("--");
                }
                subcommandArgumentsList.addAll(this.subcommandsArguments);

                it.parse(subcommandArgumentsList);
                it.execute();

                this.parsingState = new ParsingResult(it.name);
                return this.parsingState;
            }
        } catch (final ParsingException exception) {
            this.printError(exception.getMessage());
        }

        this.parsingState = new ParsingResult(this.programName);
        return this.parsingState;
    }

    /**
     * Creates a message with the usage information.
     */
    @NotNull
    public String makeUsage() {
        final @NotNull var result = new StringBuilder();

        result.append(String.format(
                "Usage: %s options_list\n",
                String.join(" ", this.fullCommandName)
        ));

        if (!this.subcommands.isEmpty()) {
            result.append("Subcommands:\n");
            this.subcommands.forEach((unused, subcommand) -> result.append(subcommand.getHelpMessage()));
        }
        if (!this.declaredArguments.isEmpty()) {
            result.append("Arguments:\n");
            this.declaredArguments.forEach(it -> {
                assert it.entity != null;
                result.append(it.entity.delegate.descriptor.getHelpMessage());
            });
        }
        if (!this.declaredOptions.isEmpty()) {
            result.append("Options:\n");
            this.declaredOptions.forEach(it -> {
                assert it.entity != null;
                result.append(it.entity.delegate.descriptor.getHelpMessage());
            });
        }
        return result.toString();
    }

    public enum ValueOrigin {
        /**
         * The value was parsed from command line arguments.
         */
        SET_BY_USER,
        /**
         * The value was missing in command line, therefore the default value was used.
         */
        SET_DEFAULT_VALUE,
        /**
         * The value is not initialized by command line values or by default values.
         */
        UNSET,
        /**
         * The value is undefined, because parsing wasn't called.
         */
        UNDEFINED
    }

    public enum OptionPrefixStyle {
        /**
         * Linux style: the full name of an option is prefixed with two hyphens "--" and the short name — with one "-".
         */
        LINUX,
        /**
         * JVM style: both full and short names are prefixed with one hyphen "-".
         */
        JVM,
        /**
         * GNU style: the full name of an option is prefixed with two hyphens "--" and "=" between options and value
         * and the short name — with one "-".
         * Detailed information https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html
         */
        GNU
    }
}

final class CLIEntityWrapper {
    @Nullable CLIEntity<?, ?> entity;

    CLIEntityWrapper() {
        this.entity = null;
    }
}

/**
 * The base class for a command line argument or an option.
 */
abstract class CLIEntity<T, TResult> implements ValueHolder<TResult> {
    protected final @NotNull ArgumentValues.ParsingValue<T, TResult> delegate;
    protected final @NotNull CLIEntityWrapper owner;

    protected CLIEntity(
            @NotNull final ArgumentValues.ParsingValue<T, TResult> delegate,
            @NotNull final CLIEntityWrapper owner) {
        this.delegate = delegate;
        this.owner = owner;
        this.owner.entity = this;
    }

    /**
     * Get value of the option or argument parsed from command line.
     * <p>
     * Calling this method before it gets its value will result in an exception.
     * You can use [getValueOrigin] property to find out whether the property has been already set.
     */
    @Override
    public @Nullable TResult getValue() {
        return this.delegate.getValue();
    }

    /**
     * The origin of the option/argument value.
     */
    @Override
    public @NotNull ArgumentParser.ValueOrigin getValueOrigin() {
        return this.delegate.getValueOrigin();
    }
}
