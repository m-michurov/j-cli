package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class Options {

    /**
     * The base class for command line options.
     * <p>
     * You can use [ArgParser.option] function to declare an option.
     */
    abstract static class Option<T, TResult> extends CLIEntity<T, TResult> {

        protected Option(
                @NotNull final ArgumentValues.ParsingValue<T, TResult> delegate,
                @NotNull final CLIEntityWrapper owner) {
            super(delegate, owner);
        }
    }

    /**
     * The base class of an option with a single value.
     */
    abstract static class AbstractSingleOption<T> extends Option<T, T> {

        protected AbstractSingleOption(
                @NotNull final ArgumentValues.ParsingValue<T, T> delegate,
                @NotNull final CLIEntityWrapper owner) {
            super(delegate, owner);
        }

        /**
         * Check descriptor for this kind of option.
         */
        protected void checkDescriptor(@NotNull final Descriptors.OptionDescriptor<?, ?> descriptor) {
            if (descriptor.multiple || descriptor.delimiter != null) {
                throw new IllegalArgumentException(
                        "Option with single value can't be initialized with descriptor for multiple values."
                );
            }
        }

        protected static <T> Descriptors.@NotNull OptionDescriptor<T, List<T>> createDescriptorForMultipleValues(
                @NotNull final Descriptors.OptionDescriptor<T, T> descriptor) {
            return new Descriptors.OptionDescriptor<>(
                    descriptor.optionFullFormPrefix,
                    descriptor.optionShortFromPrefix,
                    descriptor.type,
                    descriptor.fullName,
                    descriptor.shortName,
                    descriptor.description,
                    descriptor.defaultValue != null
                            ? Collections.singletonList(descriptor.defaultValue)
                            : Collections.emptyList(),
                    descriptor.required,
                    true,
                    descriptor.delimiter,
                    descriptor.deprecatedWarning
            );
        }

        protected static <T> Descriptors.@NotNull OptionDescriptor<T, List<T>> createDescriptorWithDelimiter(
                @NotNull final Descriptors.OptionDescriptor<T, T> descriptor,
                @Nullable final String delimiterValue) {
            return new Descriptors.OptionDescriptor<>(
                    descriptor.optionFullFormPrefix,
                    descriptor.optionShortFromPrefix,
                    descriptor.type,
                    descriptor.fullName,
                    descriptor.shortName,
                    descriptor.description,
                    descriptor.defaultValue != null
                            ? Collections.singletonList(descriptor.defaultValue)
                            : Collections.emptyList(),
                    descriptor.required,
                    descriptor.multiple,
                    delimiterValue,
                    descriptor.deprecatedWarning
            );
        }
    }

    /**
     * A required option.
     * <p>
     * The [value] of such option is non-null.
     */
    public static final class SingleRequiredOption<T> extends AbstractSingleOption<T> {
        final @NotNull Descriptors.OptionDescriptor<T, T> descriptor;

        SingleRequiredOption(
                @NotNull final Descriptors.OptionDescriptor<T, T> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentSingleValue<>(descriptor), owner);
            this.checkDescriptor(descriptor);

            this.descriptor = descriptor;
        }

        @Override
        public @NotNull T getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        /**
         * Allows the option to have several values specified in command line string.
         * Number of values is unlimited.
         */
        public @NotNull MultipleRequiredOption<T> multiple() {
            final @NotNull var newOption = new MultipleRequiredOption<>(
                    createDescriptorForMultipleValues(this.descriptor),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }

        /**
         * Allows the option to have several values joined with [delimiter] specified in command line string.
         * Number of values is unlimited.
         * <p>
         * The value of the argument is an empty list in case if no value was specified in command line string.
         *
         * @param delimiterValue delimiter used to separate string value to option values list.
         */
        public @NotNull SingleRequiredOptionWithDelimiter<T> delimiter(@NotNull final String delimiterValue) {
            final @NotNull var newOption = new SingleRequiredOptionWithDelimiter<>(
                    createDescriptorWithDelimiter(this.descriptor, delimiterValue),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }
    }

    /**
     * An option with nullable [value].
     */
    public static final class SingleNullableOption<T> extends AbstractSingleOption<T> {
        final @NotNull Descriptors.OptionDescriptor<T, T> descriptor;

        SingleNullableOption(
                @NotNull final Descriptors.OptionDescriptor<T, T> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentSingleNullableValue<>(descriptor), owner);
            this.checkDescriptor(descriptor);

            this.descriptor = descriptor;
        }

        /**
         * Requires the option to be always provided in command line.
         */
        public @NotNull SingleRequiredOption<T> required() {
            final @NotNull var newOption = new SingleRequiredOption<>(
                    new Descriptors.OptionDescriptor<>(
                            this.descriptor.optionFullFormPrefix,
                            this.descriptor.optionShortFromPrefix,
                            this.delegate.descriptor.type,
                            this.delegate.descriptor.fullName,
                            this.descriptor.shortName,
                            this.delegate.descriptor.description,
                            this.delegate.descriptor.defaultValue,
                            true,
                            this.descriptor.multiple,
                            this.descriptor.delimiter,
                            this.delegate.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }

        /**
         * Specifies the default value for the option, that will be used when no value is provided for it
         * in command line string.
         *
         * @param value the default value.
         */
        public @NotNull SingleOptionWithDefault<T> setDefault(@NotNull final T value) {
            final @NotNull var newOption = new SingleOptionWithDefault<>(
                    new Descriptors.OptionDescriptor<>(
                            this.descriptor.optionFullFormPrefix,
                            this.descriptor.optionShortFromPrefix,
                            this.delegate.descriptor.type,
                            this.delegate.descriptor.fullName,
                            this.descriptor.shortName,
                            this.delegate.descriptor.description,
                            value,
                            this.delegate.descriptor.required,
                            this.descriptor.multiple,
                            this.descriptor.delimiter,
                            this.delegate.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }

        /**
         * Allows the option to have several values specified in command line string.
         * Number of values is unlimited.
         */
        public @NotNull MultipleMaybeEmptyOption<T> multiple() {
            final @NotNull var newOption = new MultipleMaybeEmptyOption<>(
                    createDescriptorForMultipleValues(this.descriptor),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }

        /**
         * Allows the option to have several values joined with [delimiter] specified in command line string.
         * Number of values is unlimited.
         * <p>
         * The value of the argument is an empty list in case if no value was specified in command line string.
         *
         * @param delimiterValue delimiter used to separate string value to option values list.
         */
        public @NotNull SingleMaybeEmptyOptionWithDelimiter<T> delimiter(@NotNull final String delimiterValue) {
            final @NotNull var newOption = new SingleMaybeEmptyOptionWithDelimiter<>(
                    createDescriptorWithDelimiter(this.descriptor, delimiterValue),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }
    }

    /**
     * An option with a default value.
     * <p>
     * The [value] of such option is non-null.
     */
    public static final class SingleOptionWithDefault<T> extends AbstractSingleOption<T> {
        final  @NotNull Descriptors.OptionDescriptor<T, T> descriptor;

        SingleOptionWithDefault(
                @NotNull final Descriptors.OptionDescriptor<T, T> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentSingleValue<>(descriptor), owner);
            this.checkDescriptor(descriptor);

            this.descriptor = descriptor;
        }

        @Override
        protected void checkDescriptor(@NotNull final Descriptors.OptionDescriptor<?, ?> descriptor)
                throws IllegalArgumentException {
            super.checkDescriptor(descriptor);
            if (!descriptor.defaultValueSet) {
                throw new IllegalArgumentException(
                        "Argument with default value can't be initialized using descriptor without default value."
                );
            }
        }

        @Override
        public @NotNull T getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        /**
         * Allows the option to have several values specified in command line string.
         * Number of values is unlimited.
         */
        public @NotNull MultipleOptionWithDefault<T> multiple() {
            final @NotNull var newOption = new MultipleOptionWithDefault<>(
                    createDescriptorForMultipleValues(this.descriptor),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }

        /**
         * Allows the option to have several values joined with [delimiter] specified in command line string.
         * Number of values is unlimited.
         * <p>
         * The value of the argument is an empty list in case if no value was specified in command line string.
         *
         * @param delimiterValue delimiter used to separate string value to option values list.
         */
        public @NotNull SingleOptionWithDefaultWithDelimiter<T> delimiter(@NotNull final String delimiterValue) {
            final @NotNull var newOption = new SingleOptionWithDefaultWithDelimiter<>(
                    createDescriptorWithDelimiter(this.descriptor, delimiterValue),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }
    }

    /**
     * An option that allows several values to be provided in command line string.
     * <p>
     * The [value] property of such option has type `List<T>`.
     */
    public static final class MultipleRequiredOption<T> extends Option<T, List<T>> {

        MultipleRequiredOption(
                @NotNull final Descriptors.OptionDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (!descriptor.multiple && descriptor.delimiter == null) {
                throw new IllegalArgumentException(
                        "Option with multiple values can't be initialized with descriptor for single one."
                );
            }
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }
    }

    /**
     * An option that allows none values to be provided in command line string.
     * <p>
     * The [value] property of such option has type `List<T>`.
     */
    public static final class MultipleMaybeEmptyOption<T> extends Option<T, List<T>> {
        final @NotNull Descriptors.OptionDescriptor<T, List<T>> descriptor;

        MultipleMaybeEmptyOption(
                @NotNull final Descriptors.OptionDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (!descriptor.multiple && descriptor.delimiter == null) {
                throw new IllegalArgumentException(
                        "Option with multiple values can't be initialized with descriptor for single one."
                );
            }

            this.descriptor = descriptor;
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        /**
         * Specifies the default value for the option with multiple values, that will be used when no values are provided
         * for it in command line string.
         *
         * @param value the default value, must be a non-empty collection.
         * @throws IllegalArgumentException if provided default value is empty collection.
         */
        public @NotNull MultipleOptionWithDefault<T> setDefault(@NotNull final Collection<? extends T> value) {
            if (value.isEmpty()) {
                throw new IllegalArgumentException("Default value for option can't be empty collection.");
            }

            final @NotNull var newArgument = new MultipleOptionWithDefault<>(
                    new Descriptors.OptionDescriptor<>(
                            this.descriptor.optionFullFormPrefix,
                            this.descriptor.optionShortFromPrefix,
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.shortName,
                            this.descriptor.description,
                            new ArrayList<>(value),
                            this.descriptor.required,
                            this.descriptor.multiple,
                            this.descriptor.delimiter,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        /**
         * Requires the option to be always provided in command line.
         */
        public @NotNull MultipleRequiredOption<T> required() {
            final @NotNull var newOption = new MultipleRequiredOption<>(
                    new Descriptors.OptionDescriptor<>(
                            this.descriptor.optionFullFormPrefix,
                            this.descriptor.optionShortFromPrefix,
                            this.delegate.descriptor.type,
                            this.delegate.descriptor.fullName,
                            this.descriptor.shortName,
                            this.delegate.descriptor.description,
                            this.delegate.descriptor.defaultValue,
                            true,
                            this.descriptor.multiple,
                            this.descriptor.delimiter,
                            this.delegate.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }
    }

    /**
     * An option that allows several none to be provided in command line string and has a default value.
     * <p>
     * The [value] property of such option has type `List<T>`.
     */
    public static final class MultipleOptionWithDefault<T> extends Option<T, List<T>> {

        MultipleOptionWithDefault(
                @NotNull final Descriptors.OptionDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (!descriptor.multiple && descriptor.delimiter == null) {
                throw new IllegalArgumentException(
                        "Option with multiple values can't be initialized with descriptor for single one."
                );
            }
            if (!descriptor.defaultValueSet) {
                throw new IllegalArgumentException(
                        "Option with default value can't be initialized with descriptor without default value."
                );
            }
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }
    }

    public static final class SingleRequiredOptionWithDelimiter<T> extends Option<T, List<T>> {

        SingleRequiredOptionWithDelimiter(
                @NotNull final Descriptors.OptionDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (!descriptor.multiple && descriptor.delimiter == null) {
                throw new IllegalArgumentException(
                        "Option with multiple values can't be initialized with descriptor for single one."
                );
            }
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }
    }

    public static final class SingleMaybeEmptyOptionWithDelimiter<T> extends Option<T, List<T>> {
        final @NotNull Descriptors.OptionDescriptor<T, List<T>> descriptor;

        SingleMaybeEmptyOptionWithDelimiter(
                @NotNull final Descriptors.OptionDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (!descriptor.multiple && descriptor.delimiter == null) {
                throw new IllegalArgumentException(
                        "Option with multiple values can't be initialized with descriptor for single one."
                );
            }

            this.descriptor = descriptor;
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }

        /**
         * Specifies the default value for the option, that will be used when no value is provided for it
         * in command line string.
         *
         * @param value the default value.
         */
        public @NotNull SingleOptionWithDefaultWithDelimiter<T> setDefault(
                @NotNull final Collection<? extends T> value) {
            if (value.isEmpty()) {
                throw new IllegalArgumentException("Default value for option can't be empty collection.");
            }

            final @NotNull var newArgument = new SingleOptionWithDefaultWithDelimiter<>(
                    new Descriptors.OptionDescriptor<>(
                            this.descriptor.optionFullFormPrefix,
                            this.descriptor.optionShortFromPrefix,
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.shortName,
                            this.descriptor.description,
                            new ArrayList<>(value),
                            this.descriptor.required,
                            this.descriptor.multiple,
                            this.descriptor.delimiter,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newArgument;
            return newArgument;
        }

        /**
         * Requires the option to be always provided in command line.
         */
        public @NotNull SingleRequiredOptionWithDelimiter<T> required() {
            final @NotNull var newOption = new SingleRequiredOptionWithDelimiter<>(
                    new Descriptors.OptionDescriptor<>(
                            this.descriptor.optionFullFormPrefix,
                            this.descriptor.optionShortFromPrefix,
                            this.descriptor.type,
                            this.descriptor.fullName,
                            this.descriptor.shortName,
                            this.descriptor.description,
                            this.descriptor.defaultValue,
                            true,
                            this.descriptor.multiple,
                            this.descriptor.delimiter,
                            this.descriptor.deprecatedWarning
                    ),
                    this.owner
            );

            this.owner.entity = newOption;
            return newOption;
        }
    }

    public static final class SingleOptionWithDefaultWithDelimiter<T> extends Option<T, List<T>> {

        SingleOptionWithDefaultWithDelimiter(
                @NotNull final Descriptors.OptionDescriptor<T, List<T>> descriptor,
                @NotNull final CLIEntityWrapper owner) {
            super(new ArgumentValues.ArgumentMultipleValues<>(descriptor), owner);

            if (!descriptor.multiple && descriptor.delimiter == null) {
                throw new IllegalArgumentException(
                        "Option with multiple values can't be initialized with descriptor for single one."
                );
            }
            if (!descriptor.defaultValueSet) {
                throw new IllegalArgumentException(
                        "Option with default value can't be initialized with descriptor without default value."
                );
            }
        }

        @Override
        public @NotNull List<T> getValue() {
            return Objects.requireNonNull(super.getValue());
        }
    }
}
