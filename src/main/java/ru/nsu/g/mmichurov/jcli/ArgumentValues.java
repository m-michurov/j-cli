package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedList;
import java.util.List;

public class ArgumentValues {

    /**
     * Parsing value of option/argument.
     */
    abstract static class ParsingValue<T, TResult> implements ValueHolder<TResult> {
        final @NotNull Descriptors.Descriptor<T, TResult> descriptor;
        /**
         * Values of arguments.
         */
        protected @Nullable TResult parsedValue = null;
        /**
         * Value origin.
         */
        protected @NotNull ArgumentParser.ValueOrigin valueOrigin = ArgumentParser.ValueOrigin.UNDEFINED;

        protected ParsingValue(@NotNull final Descriptors.Descriptor<T, TResult> descriptor) {
            this.descriptor = descriptor;
        }

        static void printWarning(@NotNull final String message) {
            System.out.printf("WARNING %s%n", message);
        }

        /**
         * Check if values of argument are empty.
         */
        abstract boolean isEmpty();

        /**
         * Check if value of argument was not initialized.
         */
        protected boolean valueIsNotInitialized() {
            return this.parsedValue == null;
        }

        /**
         * Save value from command line.
         *
         * @param stringValue value from command line.
         */
        protected abstract void saveValue(@NotNull String stringValue) throws ParsingException;

        /**
         * Add parsed value from command line.
         *
         * @param stringValue value from command line.
         */
        void addValue(@NotNull final String stringValue) throws ParsingException {
            if (!this.isEmpty() && !this.descriptor.allowsMultipleValues()) {
                throw new ParsingException(
                        String.format("Try to provide more than one value for %s.", this.descriptor.fullName)
                );
            }

            if (this.descriptor.deprecatedWarning != null) {
                if (this.isEmpty()) {
                    printWarning(this.descriptor.deprecatedWarning);
                }
            }

            for (@NotNull final var it : this.descriptor.splitIfNeeded(stringValue)) {
                this.saveValue(it);
            }
        }

        /**
         * Set default value to option.
         */
        void addDefaultValue() {
            if (this.descriptor.defaultValueSet) {
                assert this.descriptor.defaultValue != null;
                this.parsedValue = this.descriptor.defaultValue;
                this.valueOrigin = ArgumentParser.ValueOrigin.SET_DEFAULT_VALUE;
            }
        }

        @Override
        public @NotNull ArgumentParser.ValueOrigin getValueOrigin() {
            return this.valueOrigin;
        }
    }

    /**
     * Single argument value.
     */
    abstract static class AbstractArgumentSingleValue<T> extends ParsingValue<T, T> {

        /**
         * Single argument value.
         *
         * @param descriptor descriptor of option/argument.
         */
        protected AbstractArgumentSingleValue(@NotNull final Descriptors.Descriptor<T, T> descriptor) {
            super(descriptor);
        }

        @Override
        protected void saveValue(@NotNull final String stringValue) throws ParsingException {
            if (this.valueIsNotInitialized()) {
                this.parsedValue = this.descriptor.type.convert(stringValue, this.descriptor.fullName);
                this.valueOrigin = ArgumentParser.ValueOrigin.SET_BY_USER;
            } else {
                throw new ParsingException(
                        String.format(
                                "Try to provide more than one value %s and %s for %s.",
                                this.parsedValue, stringValue, this.descriptor.fullName
                        )
                );
            }
        }

        @Override
        boolean isEmpty() {
            return this.valueIsNotInitialized();
        }
    }

    /**
     * Single argument value.
     */
    static final class ArgumentSingleValue<T>
            extends AbstractArgumentSingleValue<T>
            implements ValueHolder<@NotNull T> {

        /**
         * Single argument value.
         *
         * @param descriptor descriptor of option/argument.
         */
        protected ArgumentSingleValue(@NotNull final Descriptors.Descriptor<T, T> descriptor) {
            super(descriptor);
        }

        @Override
        public @NotNull T getValue() {
            if (this.isEmpty()) {
                throw new IllegalStateException(
                        String.format(
                                "Value for argument %s isn't set. ArgParser.parse(...) method should be called before.",
                                this.descriptor.fullName
                        )
                );
            } else {
                assert this.parsedValue != null;
                return this.parsedValue;
            }
        }
    }

    /**
     * Single nullable argument value.
     */
    static final class ArgumentSingleNullableValue<T>
            extends AbstractArgumentSingleValue<T>
            implements ValueHolder<@Nullable T> {

        /**
         * Single nullable argument value.
         *
         * @param descriptor descriptor of option/argument.
         */
        protected ArgumentSingleNullableValue(@NotNull final Descriptors.Descriptor<T, T> descriptor) {
            super(descriptor);
        }

        @Override
        public @Nullable T getValue() {
            return this.isEmpty() ? null : this.parsedValue;
        }
    }

    /**
     * Multiple argument values.
     */
    static final class ArgumentMultipleValues<T>
            extends ParsingValue<T, List<T>>
            implements ValueHolder<@NotNull List<T>> {
        private final List<T> addedValue = new LinkedList<>();

        /**
         * Multiple argument values.
         *
         * @param descriptor descriptor of option/argument.
         */
        protected ArgumentMultipleValues(@NotNull final Descriptors.Descriptor<T, List<T>> descriptor) {
            super(descriptor);

            this.parsedValue = this.addedValue;
        }

        @Override
        boolean isEmpty() {
            assert this.parsedValue != null;
            return this.parsedValue.isEmpty();
        }

        @Override
        protected void saveValue(@NotNull final String stringValue) throws ParsingException {
            this.addedValue.add(this.descriptor.type.convert(stringValue, this.descriptor.fullName));
            this.valueOrigin = ArgumentParser.ValueOrigin.SET_BY_USER;
        }

        @Override
        public @NotNull List<T> getValue() {
            assert this.parsedValue != null;
            return this.parsedValue;
        }
    }
}
