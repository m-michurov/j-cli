package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Descriptors {

    /**
     * Common descriptor both for options and positional arguments.
     */
    abstract static class Descriptor<T, TResult> {
        final @NotNull ArgType<T> type;
        final @NotNull String fullName;
        final @Nullable String description;
        final @Nullable TResult defaultValue;
        final boolean required;
        final @Nullable String deprecatedWarning;

        /**
         * Flag to check if descriptor has set default value for option/argument.
         */
        final boolean defaultValueSet;

        /**
         * Common descriptor both for options and positional arguments.
         *
         * @param type              option/argument type, one of [ArgType].
         * @param fullName          option/argument full name.
         * @param description       text description of option/argument.
         * @param defaultValue      default value for option/argument.
         * @param required          if option/argument is required or not. If it's required and not provided in command line, error will be generated.
         * @param deprecatedWarning text message with information in case if option is deprecated.
         */
        protected Descriptor(
                @NotNull final ArgType<T> type,
                @NotNull final String fullName,
                @Nullable final String description,
                @Nullable final TResult defaultValue,
                final boolean required,
                @Nullable final String deprecatedWarning) {
            this.type = type;
            this.fullName = fullName;
            this.description = description;
            this.defaultValue = defaultValue;
            this.required = required;
            this.deprecatedWarning = deprecatedWarning;
            this.defaultValueSet = defaultValue != null && (
                    !(defaultValue instanceof List<?>) || !((Collection<?>) defaultValue).isEmpty()
            );
        }

        /**
         * Text description for help message.
         */
        abstract @NotNull String getTextDescription();

        /**
         * Help message for descriptor.
         */
        abstract @NotNull String getHelpMessage();

        /**
         * Provide text description of value.
         *
         * @param value value got getting text description for.
         */
        @Nullable String getValueDescription(@Nullable final TResult value) {
            if (value == null) {
                return null;
            }

            if (value instanceof List<?>) {
                return ((Collection<?>) value).isEmpty() ? null : String.format(" %s", value);
            }

            return String.format(" [%s]", value);
        }

        abstract boolean allowsMultipleValues();

        abstract @NotNull List<String> splitIfNeeded(@NotNull String stringValue);
    }

    /**
     * Option descriptor.
     * <p>
     * Command line entity started with some prefix (-/--) and can have value as next entity in command line string.
     */
    static final class OptionDescriptor<T, TResult> extends Descriptor<T, TResult> {
        final @NotNull String optionFullFormPrefix;
        final @NotNull String optionShortFromPrefix;
        final @Nullable String shortName;
        final boolean multiple;
        final @Nullable String delimiter;

        /**
         * Option descriptor.
         * <p>
         * Command line entity started with some prefix (-/--) and can have value as next entity in command line string.
         *
         * @param optionFullFormPrefix  prefix used before full form of option.
         * @param optionShortFromPrefix prefix used before short form of option.
         * @param type                  option type, one of [ArgType].
         * @param fullName              option full name.
         * @param shortName             option short name.
         * @param description           text description of option.
         * @param defaultValue          default value for option.
         * @param required              if option is required or not. If it's required and not provided in command line, error will be generated.
         * @param multiple              if option can be repeated several times in command line with different values. All values are stored.
         * @param delimiter             delimiter that separate option provided as one string to several values.
         * @param deprecatedWarning     text message with information in case if option is deprecated.
         */
        OptionDescriptor(
                @NotNull final String optionFullFormPrefix,
                @NotNull final String optionShortFromPrefix,
                @NotNull final ArgType<T> type,
                @NotNull final String fullName,
                @Nullable final String shortName,
                @Nullable final String description,
                @Nullable final TResult defaultValue,
                final boolean required,
                final boolean multiple,
                @Nullable final String delimiter,
                @Nullable final String deprecatedWarning) {
            super(type, fullName, description, defaultValue, required, deprecatedWarning);

            this.optionFullFormPrefix = optionFullFormPrefix;
            this.optionShortFromPrefix = optionShortFromPrefix;
            this.shortName = shortName;
            this.multiple = multiple;
            this.delimiter = delimiter;
        }

        @Override
        @NotNull String getTextDescription() {
            return String.format("option %s%s", this.optionFullFormPrefix, this.fullName);
        }

        @Override
        @NotNull String getHelpMessage() {
            final var result = new StringBuilder();
            result.append(String.format("    %s%s", this.optionFullFormPrefix, this.fullName));
            if (this.shortName != null) {
                result.append(String.format(", %s%s", this.optionShortFromPrefix, this.shortName));
            }
            final @Nullable var defaultValueDescription = this.getValueDescription(this.defaultValue);
            if (defaultValueDescription != null) {
                result.append(defaultValueDescription);
            }
            if (this.description != null) {
                result.append(String.format(" -> %s", this.description));
            }
            if (this.required) {
                result.append(" (always required)");
            }
            result.append(String.format(" %s", this.type.getDescription()));
            if (this.deprecatedWarning != null) {
                result.append(String.format(" Warning: %s", this.deprecatedWarning));
            }
            result.append('\n');
            return result.toString();
        }

        @Override
        boolean allowsMultipleValues() {
            return this.multiple || this.delimiter != null;
        }

        @Override
        @NotNull List<String> splitIfNeeded(@NotNull final String stringValue) {
            return this.delimiter != null
                    ? Arrays.asList(stringValue.split(this.delimiter))
                    : Collections.singletonList(stringValue);
        }
    }

    /**
     * Argument descriptor.
     * <p>
     * Command line entity which role is connected only with its position.
     */
    static final class ArgumentDescriptor<T, TResult> extends Descriptor<T, TResult> {
        final @Nullable Integer num;

        /**
         * Argument descriptor.
         * <p>
         * Command line entity which role is connected only with its position.
         *
         * @param type              argument type, one of [ArgType].
         * @param fullName          argument full name.
         * @param num               expected number of values. Null means any possible number of values.
         * @param description       text description of argument.
         * @param defaultValue      default value for argument.
         * @param required          if argument is required or not. If it's required and not provided in command line and have no default value, error will be generated.
         * @param deprecatedWarning text message with information in case if argument is deprecated.
         */
        ArgumentDescriptor(
                @NotNull final ArgType<T> type,
                @NotNull final String fullName,
                @Nullable final Integer num,
                @Nullable final String description,
                @Nullable final TResult defaultValue,
                final boolean required,
                @Nullable final String deprecatedWarning) throws IllegalArgumentException {
            super(type, fullName, description, defaultValue, required, deprecatedWarning);

            if (num != null && num < 1) {
                throw new IllegalArgumentException(
                        String.format(
                                "Number of arguments for argument description %s should be greater than zero.",
                                this.fullName
                        )
                );
            }

            this.num = num;
        }

        @Override
        @NotNull String getTextDescription() {
            return String.format("argument %s", this.fullName);
        }

        @Override
        @NotNull String getHelpMessage() {
            final var result = new StringBuilder();
            result.append(String.format("    %s", this.fullName));
            final @Nullable var defaultValueDescription = this.getValueDescription(this.defaultValue);
            if (defaultValueDescription != null) {
                result.append(defaultValueDescription);
            }
            if (this.description != null) {
                result.append(String.format(" -> %s", this.description));
            }
            if (!this.required) {
                result.append(" (optional)");
            }
            result.append(String.format(" %s", this.type.getDescription()));
            if (this.deprecatedWarning != null) {
                result.append(String.format(" Warning: %s", this.deprecatedWarning));
            }
            result.append('\n');
            return result.toString();
        }

        @Override
        boolean allowsMultipleValues() {
            return this.num == null || this.num > 1;
        }

        @Override
        @NotNull List<String> splitIfNeeded(@NotNull final String stringValue) {
            return Collections.singletonList(stringValue);
        }
    }
}
