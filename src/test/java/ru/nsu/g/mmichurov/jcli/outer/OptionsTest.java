package ru.nsu.g.mmichurov.jcli.outer;

import ru.nsu.g.mmichurov.jcli.ArgType;
import ru.nsu.g.mmichurov.jcli.ArgumentParser;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class OptionsTest {

    @Test
    public void testShortForm() {
        final var argParser = new ArgumentParser("testParser");
        final var output = argParser.option(
                ArgType.String, "output", "o", "Output file"
        );
        final var input = argParser.option(
                ArgType.String, "input", "i", "Input file"
        );
        argParser.parse("-o", "out.txt", "-i", "input.txt");
        assertEquals("out.txt", output.getValue());
        assertEquals("input.txt", input.getValue());
    }

    @Test
    public void testFullForm() {
        final var argParser = new ArgumentParser("testParser");
        final var output = argParser.option(
                ArgType.String, "output", "o", "Output file"
        );
        final var input = argParser.option(
                ArgType.String, "input", "i", "Input file"
        );
        argParser.parse("--output", "out.txt", "--input", "input.txt");
        assertEquals("out.txt", output.getValue());
        assertEquals("input.txt", input.getValue());
    }

    @Test
    public void testJavaPrefix() {
        final var argParser = new ArgumentParser("testParser", ArgumentParser.OptionPrefixStyle.JVM);
        final var output = argParser.option(
                ArgType.String, "output", "o", "Output file"
        );
        final var input = argParser.option(
                ArgType.String, "input", "i", "Input file"
        );
        argParser.parse("-output", "out.txt", "-i", "input.txt");
        assertEquals("out.txt", output.getValue());
        assertEquals("input.txt", input.getValue());
    }

    enum Renders {
        TEXT,
        HTML,
        XML,
        JSON
    }

    @Test
    public void testGNUPrefix() {
        final var argParser = new ArgumentParser("testParser", ArgumentParser.OptionPrefixStyle.GNU);
        final var output = argParser.option(
                ArgType.String, "output", "o", "Output file"
        );
        final var input = argParser.option(
                ArgType.String, "input", "i", "Input file"
        );
        final var verbose = argParser.flag(
                "verbose", "v", "Verbose print"
        );
        final var shortForm = argParser.flag(
                "short", "s", "Short output form"
        );
        final var text = argParser.flag(
                "text", "t", "Use text format"
        );
        argParser.parse("-oout.txt", "-s", "--input=input.txt", "-vt");
        assertEquals("out.txt", output.getValue());
        assertEquals("input.txt", input.getValue());
        assertTrue(verbose.getValue());
        assertTrue(shortForm.getValue());
        assertTrue(text.getValue());
    }

    @Test
    public void testGNUArguments() {
        final var argParser = new ArgumentParser("testParser", ArgumentParser.OptionPrefixStyle.GNU);
        final var output = argParser.argument(
                ArgType.String, "output", "Output file"
        );
        final var input = argParser.argument(
                ArgType.String, "input", "Input file"
        );
        final var verbose = argParser.option(
                ArgType.Boolean, "verbose", "v", "Verbose print"
        );
        final var shortForm = argParser.flag(
                "short", "s", "Short output form"
        );
        final var text = argParser.flag(
                "text", "t", "Use text format"
        );
        argParser.parse("--verbose", "--", "out.txt", "--input.txt");

        assertEquals("out.txt", output.getValue());
        assertEquals("--input.txt", input.getValue());
        assertEquals(ArgumentParser.ValueOrigin.SET_BY_USER, verbose.getValueOrigin());
        assertTrue(Objects.requireNonNull(verbose.getValue()));
        assertFalse(shortForm.getValue());
        assertFalse(text.getValue());
    }

    @Test
    public void testMultipleOptions() {
        final var argParser = new ArgumentParser("testParser");
        final var useShortForm = argParser.option(
                ArgType.Boolean, "short", "s", "Show short version of report"
        ).setDefault(false);

        final var renders = argParser.option(
                ArgType.choiceOf(Renders.class), "renders", "r", "Renders for showing information"
        ).multiple().setDefault(Collections.singletonList(Renders.TEXT));

        final var sources = argParser.option(
                ArgType.choiceOf(DataSourceEnum.class), "sources", "ds", "Data sources"
        ).multiple().setDefault(Collections.singletonList(DataSourceEnum.PRODUCTION));

        argParser.parse("-s", "-r", "text", "-r", "json", "-ds", "local", "-ds", "production");

        assertTrue(useShortForm.getValue());

        assertEquals(2, renders.getValue().size());
        final var firstRender = renders.getValue().get(0);
        final var secondRender = renders.getValue().get(1);
        assertEquals(Renders.TEXT, firstRender);
        assertEquals(Renders.JSON, secondRender);

        assertEquals(2, sources.getValue().size());
        final var firstSource = sources.getValue().get(0);
        final var secondSource = sources.getValue().get(1);
        assertEquals(DataSourceEnum.LOCAL, firstSource);
        assertEquals(DataSourceEnum.PRODUCTION, secondSource);
    }

    @Test
    public void testDefaultOptions() {
        final var argParser = new ArgumentParser("testParser");
        final var useShortForm = argParser.option(
                ArgType.Boolean, "short", "s", "Show short version of report"
        ).setDefault(false);

        final var renders = argParser.option(
                ArgType.choiceOf(Renders.class), "renders", "r", "Renders for showing information"
        ).multiple().setDefault(Collections.singletonList(Renders.TEXT));

        final var sources = argParser.option(
                ArgType.choiceOf(DataSourceEnum.class), "sources", "ds", "Data sources"
        ).multiple().setDefault(Collections.singletonList(DataSourceEnum.PRODUCTION));

        argParser.option(
                ArgType.String, "output", "o", "Output file"
        );
        argParser.parse("-o", "out.txt");

        assertFalse(useShortForm.getValue());
        assertEquals(Renders.TEXT, renders.getValue().get(0));
        assertEquals(DataSourceEnum.PRODUCTION, sources.getValue().get(0));
    }
}