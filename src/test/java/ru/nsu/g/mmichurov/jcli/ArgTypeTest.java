package ru.nsu.g.mmichurov.jcli;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class ArgTypeTest {

    @Test
    public void Boolean() throws ParsingException {
        assertEquals("", ArgType.Boolean.getDescription());
        assertTrue(ArgType.Boolean.convert("", ""));
        assertTrue(ArgType.Boolean.convert("abc", ""));
        assertFalse(ArgType.Boolean.convert("false", ""));
    }

    @Test
    public void String() {
        assertEquals("{ String }", ArgType.String.getDescription());
        assertDoesNotThrow(() -> assertEquals("", ArgType.String.convert("", "a")));
        assertDoesNotThrow(() -> assertEquals("abc", ArgType.String.convert("abc", "b")));
        assertDoesNotThrow(() -> assertEquals("false", ArgType.String.convert("false", "c")));
    }

    @Test
    public void Integer() {
        assertEquals("{ Integer }", ArgType.Integer.getDescription());

        final var nothrowTests = new ValueNameEntry[]{
                entry("123", "a"),
                entry("77", "b"),
                entry("-666", "c")
        };

        for (final var e : nothrowTests) {
            final var value = e.value;
            final var name = e.name;
            assertDoesNotThrow(() -> assertEquals(Integer.parseInt(value), ArgType.Integer.convert(value, name)));
        }

        final var throwTests = new ValueNameEntry[]{
                entry("abc", "a"),
                entry("1.1", "b"),
                entry(".0", "c"),
                entry("0.", "d"),
                entry("", "e")
        };

        for (final var e : throwTests) {
            final var value = e.value;
            final var name = e.name;

            final var ex = assertThrows(
                    ParsingException.class, () -> ArgType.Integer.convert(value, name)
            );
            assertEquals(
                    String.format("Option %s is expected to be integer number. %s is provided.", name, value),
                    ex.getMessage()
            );
        }
    }

    @Test
    public void Double() throws ParsingException {
        assertEquals("{ Double }", ArgType.Double.getDescription());

        final var nothrowTests = new ValueNameEntry[]{
                entry("123", "a"),
                entry("77", "b"),
                entry("-666", "c")
        };

        for (final var e : nothrowTests) {
            final var value = e.value;
            final var name = e.name;
            assertEquals(Double.parseDouble(value), ArgType.Double.convert(value, name));
        }

        final var throwTests = new ValueNameEntry[]{
                entry("abc", "a"),
                entry("1p.1", "b"),
                entry(".0dd", "c"),
                entry("0.0.", "d"),
                entry("", "e"),
                entry("f0.", "f")
        };

        for (final var e : throwTests) {
            final var value = e.value;
            final var name = e.name;

            final var ex = assertThrows(
                    ParsingException.class, () -> ArgType.Double.convert(value, name)
            );

            assertEquals(
                    String.format("Option %s is expected to be double number. %s is provided.", name, value),
                    ex.getMessage()
            );
        }
    }

    @Test
    void choiceOfEnum() {
        final var choiceArg = ArgType.choiceOf(TEST_ENUM.class);
        final var entriesAsStrings = Arrays.stream(TEST_ENUM.values())
                .map(TEST_ENUM::toString)
                .map(java.lang.String::toLowerCase)
                .collect(Collectors.toSet());
        final var choicesListStr = "[" + java.lang.String.join(", ", entriesAsStrings) + "]";

        assertEquals(
                java.lang.String.format("{ Value should be one of %s }", choicesListStr),
                choiceArg.getDescription()
        );

        final var nothrowTests = Arrays.stream(TEST_ENUM.values())
                .collect(Collectors.toMap((e) -> e, (e) -> e.toString().toLowerCase()));

        nothrowTests.forEach(
                (entry, entryAsString) -> assertDoesNotThrow(
                        () -> assertEquals(entry, choiceArg.convert(entryAsString, ""))
                )
        );

        final var throwTests = new ValueNameEntry[]{
                entry("E1", "a"),
                entry("aaa", "b"),
                entry("", "c")
        };

        for (final var e : throwTests) {
            final var value = e.value;
            final var name = e.name;

            final var ex = assertThrows(
                    ParsingException.class, () -> choiceArg.convert(value, name)
            );
            assertEquals(
                    String.format(
                            "Option %s is expected to be one of %s. %s is provided.", name, choicesListStr, value
                    ),
                    ex.getMessage()
            );
        }

        final var ex = assertThrows(
                IllegalArgumentException.class, () -> ArgType.choiceOf(BAD_TEST_ENUM.class)
        );
        assertEquals("Command line representations of enum choices are not distinct", ex.getMessage());
    }

    @Test
    void choiceOfStrings() {
        final var collectionOfStrings = Arrays.asList("a", "B", "c", "d");

        final var choiceArg = ArgType.choiceOf(collectionOfStrings);
        final var entriesAsStrings = collectionOfStrings.stream()
                .map(String::toLowerCase)
                .collect(Collectors.toSet());
        final var choicesListStr = "[" + java.lang.String.join(", ", entriesAsStrings) + "]";

        assertEquals(
                java.lang.String.format("{ Value should be one of %s }", choicesListStr),
                choiceArg.getDescription()
        );

        final var nothrowTests = collectionOfStrings.stream()
                .collect(Collectors.toMap((e) -> e, String::toLowerCase));

        nothrowTests.forEach(
                (entry, entryAsString) -> assertDoesNotThrow(
                        () -> assertEquals(entry, choiceArg.convert(entryAsString, ""))
                )
        );

        final var throwTests = new ValueNameEntry[]{
                entry("E1", "a"),
                entry("aaa", "b"),
                entry("", "c")
        };

        for (final var e : throwTests) {
            final var value = e.value;
            final var name = e.name;

            final var ex = assertThrows(
                    ParsingException.class, () -> choiceArg.convert(value, name)
            );
            assertEquals(
                    String.format(
                            "Option %s is expected to be one of %s. %s is provided.", name, choicesListStr, value
                    ),
                    ex.getMessage()
            );
        }
    }

    @Test
    void choiceOfIntegers() {
        final var collectionOfIntegers = Arrays.asList(1, 2, 3, 4);


        final var choiceArg = ArgType.choiceOf(collectionOfIntegers);
        final var entriesAsStrings = collectionOfIntegers.stream()
                .map((e) -> e.toString().toLowerCase())
                .collect(Collectors.toSet());
        final var choicesListStr = "[" + java.lang.String.join(", ", entriesAsStrings) + "]";

        assertEquals(
                java.lang.String.format("{ Value should be one of %s }", choicesListStr),
                choiceArg.getDescription()
        );

        final var nothrowTests = collectionOfIntegers.stream()
                .collect(Collectors.toMap((e) -> e, (e) -> e.toString().toLowerCase()));

        nothrowTests.forEach(
                (entry, entryAsString) -> assertDoesNotThrow(
                        () -> assertEquals(entry, choiceArg.convert(entryAsString, ""))
                )
        );

        final var throwTests = new ValueNameEntry[]{
                entry("E1", "a"),
                entry("aaa", "b"),
                entry("", "c")
        };

        for (final var e : throwTests) {
            final var value = e.value;
            final var name = e.name;

            final var ex = assertThrows(
                    ParsingException.class, () -> choiceArg.convert(value, name)
            );
            assertEquals(
                    String.format(
                            "Option %s is expected to be one of %s. %s is provided.", name, choicesListStr, value
                    ),
                    ex.getMessage()
            );
        }
    }

    @Test
    void choiceOfBadCollection() {
        final var badCollectionOfStrings = Arrays.asList("A", "a", "b", "c");

        final var ex = assertThrows(
                IllegalArgumentException.class, () -> ArgType.choiceOf(badCollectionOfStrings)
        );
        assertEquals("Command line representations of enum choices are not distinct", ex.getMessage());
    }

    @Test
    void choiceOfCollection() {

        assertDoesNotThrow(() -> {
        });

        final var collectionOfIntegers = Arrays.asList(1, 2, 3, 4);

        assertDoesNotThrow(() -> {
            final var choiceArg = ArgType.choiceOf(collectionOfIntegers);
            final var entriesAsStrings = collectionOfIntegers.stream()
                    .map((e) -> e.toString().toLowerCase())
                    .collect(Collectors.toSet());
            final var choicesListStr = "[" + java.lang.String.join(", ", entriesAsStrings) + "]";

            assertEquals(
                    java.lang.String.format("{ Value should be one of %s }", choicesListStr),
                    choiceArg.getDescription()
            );

            final var nothrowTests = collectionOfIntegers.stream()
                    .collect(Collectors.toMap((e) -> e, (e) -> e.toString().toLowerCase()));

            nothrowTests.forEach(
                    (entry, entryAsString) -> assertDoesNotThrow(
                            () -> assertEquals(entry, choiceArg.convert(entryAsString, ""))
                    )
            );

            final var throwTests = new ValueNameEntry[]{
                    entry("E1", "a"),
                    entry("aaa", "b"),
                    entry("", "c")
            };

            for (final var e : throwTests) {
                final var value = e.value;
                final var name = e.name;

                final var ex = assertThrows(
                        ParsingException.class, () -> choiceArg.convert(value, name)
                );
                assertEquals(
                        String.format(
                                "Option %s is expected to be one of %s. %s is provided.", name, choicesListStr, value
                        ),
                        ex.getMessage()
                );
            }
        });

        final var badCollectionOfStrings = Arrays.asList("A", "a", "b", "c");

        final var ex = assertThrows(
                IllegalArgumentException.class, () -> ArgType.choiceOf(badCollectionOfStrings)
        );
        assertEquals("Command line representations of enum choices are not distinct", ex.getMessage());
    }

    @Test
    void choiceOfVararg() {
        final var collectionOfStrings = Arrays.asList("a", "B", "c", "d");

        assertDoesNotThrow(() -> {
            final var choiceArg = ArgType.choiceOf(collectionOfStrings.toArray());
            final var entriesAsStrings = collectionOfStrings.stream()
                    .map(String::toLowerCase)
                    .collect(Collectors.toSet());
            final var choicesListStr = "[" + java.lang.String.join(", ", entriesAsStrings) + "]";

            assertEquals(
                    java.lang.String.format("{ Value should be one of %s }", choicesListStr),
                    choiceArg.getDescription()
            );

            final var nothrowTests = collectionOfStrings.stream()
                    .collect(Collectors.toMap((e) -> e, String::toLowerCase));

            nothrowTests.forEach(
                    (entry, entryAsString) -> assertDoesNotThrow(
                            () -> assertEquals(entry, choiceArg.convert(entryAsString, ""))
                    )
            );

            final var throwTests = new ValueNameEntry[]{
                    entry("E1", "a"),
                    entry("aaa", "b"),
                    entry("", "c")
            };

            for (final var e : throwTests) {
                final var value = e.value;
                final var name = e.name;

                final var ex = assertThrows(
                        ParsingException.class, () -> choiceArg.convert(value, name)
                );
                assertEquals(
                        String.format(
                                "Option %s is expected to be one of %s. %s is provided.", name, choicesListStr, value
                        ),
                        ex.getMessage()
                );
            }
        });

        final var collectionOfIntegers = Arrays.asList(1, 2, 3, 4);

        assertDoesNotThrow(() -> {
            final var choiceArg = ArgType.choiceOf(collectionOfIntegers.toArray());
            final var entriesAsStrings = collectionOfIntegers.stream()
                    .map((e) -> e.toString().toLowerCase())
                    .collect(Collectors.toSet());
            final var choicesListStr = "[" + java.lang.String.join(", ", entriesAsStrings) + "]";

            assertEquals(
                    java.lang.String.format("{ Value should be one of %s }", choicesListStr),
                    choiceArg.getDescription()
            );

            final var nothrowTests = collectionOfIntegers.stream()
                    .collect(Collectors.toMap((e) -> e, (e) -> e.toString().toLowerCase()));

            nothrowTests.forEach(
                    (entry, entryAsString) -> assertDoesNotThrow(
                            () -> assertEquals(entry, choiceArg.convert(entryAsString, ""))
                    )
            );

            final var throwTests = new ValueNameEntry[]{
                    entry("E1", "a"),
                    entry("aaa", "b"),
                    entry("", "c")
            };

            for (final var e : throwTests) {
                final var value = e.value;
                final var name = e.name;

                final var ex = assertThrows(
                        ParsingException.class, () -> choiceArg.convert(value, name)
                );
                assertEquals(
                        String.format(
                                "Option %s is expected to be one of %s. %s is provided.", name, choicesListStr, value
                        ),
                        ex.getMessage()
                );
            }
        });

        final var badArrayOfStrings = new String[]{"A", "a", "b", "c"};

        final var ex = assertThrows(
                IllegalArgumentException.class, () -> ArgType.choiceOf(badArrayOfStrings)
        );
        assertEquals("Command line representations of enum choices are not distinct", ex.getMessage());
    }

    private static @NotNull ValueNameEntry entry(@NotNull final String value, @NotNull final String name) {
        return new ValueNameEntry(value, name);
    }

    private static final class ValueNameEntry {
        public final @NotNull String value;
        public final @NotNull String name;


        private ValueNameEntry(@NotNull final String value, @NotNull final String name) {
            this.value = value;
            this.name = name;
        }
    }

    private enum TEST_ENUM {
        E1, E2, E3, E4;

        @Override
        public String toString() {
            return switch (this) {
                case E1 -> "E1";
                case E2 -> "E2";
                case E3 -> "E3";
                case E4 -> "E4";
            };
        }
    }

    private enum BAD_TEST_ENUM {
        E1, E2, E3, E4;

        @Override
        public String toString() {
            return switch (this) {
                case E1, E2 -> "E1";
                case E3 -> "E3";
                case E4 -> "E4";
            };
        }
    }
}