package ru.nsu.g.mmichurov.jcli.outer;

import ru.nsu.g.mmichurov.jcli.ArgType;
import ru.nsu.g.mmichurov.jcli.ArgumentParser;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;

import static org.junit.jupiter.api.Assertions.*;

public class ErrorTest {

    @Test
    public void testExtraArguments() {
        final var argParser = new ArgumentParser("testParser");
        argParser.argument(
                ArgType.Integer, "addendums", null, "Addendums"
        ).multiple(2);
        argParser.argument(
                ArgType.String, "output", "Output file"
        );
        argParser.option(
                ArgType.Boolean, "debug", "d", "Debug mode"
        );
        final var exception = assertThrows(
                IllegalStateException.class,
                () -> argParser.parse("2", "-d", "3", "out.txt", "something", "else", "in", "string")
        );
        assertTrue(exception.getMessage().contains("Too many arguments! Couldn't process argument something"));
    }

    @Test
    public void testUnknownOption() {
        final var argParser = new ArgumentParser("testParser");
        argParser.option(
                ArgType.String, "output", "o", "Output file");
        argParser.option(
                ArgType.String, "input", "i", "Input file");
        final var exception = assertThrows(
                IllegalStateException.class,
                () -> argParser.parse("-o", "out.txt", "-d", "-i", "input.txt")
        );
        assertTrue(exception.getMessage().contains(
                "Unknown option -d"
        ));
    }

    @Test
    public void testWrongFormat() {
        final var argParser = new ArgumentParser("testParser");
        argParser.option(
                ArgType.Integer, "number", null, "Integer number"
        );
        final var exception = assertThrows(
                IllegalStateException.class,
                () -> argParser.parse("--number", "out.txt")
        );
        assertTrue(exception.getMessage().contains(
                "Option number is expected to be integer number. out.txt is provided."
        ));
    }

    enum RenderEnum {
        TEXT,
        HTML
    }

    @Test
    public void testWrongChoice() {
        final var argParser = new ArgumentParser("testParser");
        argParser.option(
                ArgType.Boolean, "short", "s", "Show short version of report"
        ).setDefault(false);
        argParser.option(
                ArgType.choiceOf(RenderEnum.class), "renders", "r", "Renders for showing information"
        ).multiple().setDefault(Collections.singletonList(RenderEnum.TEXT));
        final var exception = assertThrows(
                IllegalStateException.class,
                () -> argParser.parse("-r", "xml")
        );
        assertTrue(exception.getMessage().contains(
                String.format(
                        "Option renders is expected to be one of %s. xml is provided.",
                        Arrays.toString(EnumSet.allOf(RenderEnum.class).stream()
                                .map(it -> it.toString().toLowerCase())
                                .sorted()
                                .toArray()
                        )
                )
        ));
    }

    @Test
    public void testWrongEnumChoice() {
        final var argParser = new ArgumentParser("testParser");
        argParser.option(
                ArgType.choiceOf(DataSourceEnum.class), "sources", "s", "Data sources"
        ).multiple().setDefault(Collections.singletonList(DataSourceEnum.PRODUCTION));
        final var exception = assertThrows(
                IllegalStateException.class,
                () -> argParser.parse("-s", "debug")
        );
        assertTrue(exception.getMessage().contains(
                String.format(
                        "Option sources is expected to be one of %s. debug is provided.",
                        Arrays.toString(EnumSet.allOf(DataSourceEnum.class).stream()
                                .map(it -> it.toString().toLowerCase())
                                .sorted()
                                .toArray()
                        )
                )
        ));
    }
}
