package ru.nsu.g.mmichurov.jcli.outer;

import ru.nsu.g.mmichurov.jcli.ArgType;
import ru.nsu.g.mmichurov.jcli.ArgumentParser;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArgumentsTest {

    @Test
    public void testPositionalArguments() {
        final var argParser = new ArgumentParser("testParser");
        final var debugMode = argParser.option(
                ArgType.Boolean, "debug", "d", "Debug mode"
        );
        final var input = argParser.argument(
                ArgType.String, "input", "Input file"
        );
        final var output = argParser.argument(
                ArgType.String, "output", "Output file"
        );

        argParser.parse("-d", "input.txt", "out.txt");

        assertEquals(true, debugMode.getValue());
        assertEquals("out.txt", output.getValue());
        assertEquals("input.txt", input.getValue());
    }

    @Test
    public void testOptionalPositionalArguments() {
        final var argParser = new ArgumentParser("testParser");
        final var debugMode = argParser.option(
                ArgType.Boolean, "debug", "d", "Debug mode"
        );
        final var input = argParser.argument(
                ArgType.String, "input", "Input file"
        );
        final var output = argParser.argument(
                ArgType.String, "output", "Output file"
        ).optional();

        argParser.parse("-d", "input.txt");

        assertEquals(true, debugMode.getValue());
        assertEquals("input.txt", input.getValue());
        assertNull(output.getValue());
    }

    @Test
    public void testArgumentsWithAnyNumberOfValues() {
        final var argParser = new ArgumentParser("testParser");
        final var output = argParser.argument(
                ArgType.String, "output", "Output file"
        );
        final var inputs = argParser.argument(
                ArgType.String, "inputs", "Input files"
        ).vararg();

        argParser.parse("out.txt", "input1.txt", "input2.txt", "input3.txt", "input4.txt");

        assertEquals("out.txt", output.getValue());
        assertEquals(4, inputs.getValue().size());
    }

    @Test
    public void testArgumentsWithSeveralValues() {
        final var argParser = new ArgumentParser("testParser");
        final var addedNumbers = argParser.argument(
                ArgType.Integer, "addedNumbers", "Added numbers"
        ).multiple(2);
        final var output = argParser.argument(
                ArgType.String, "output", "Output file"
        );
        argParser.option(
                ArgType.Boolean, "debug", "d", "Debug mode"
        );
        argParser.parse("2", "-d", "3", "out.txt");

        assertEquals("out.txt", output.getValue());
        assertEquals(2, addedNumbers.getValue().size());
        final var first = addedNumbers.getValue().get(0);
        final var second = addedNumbers.getValue().get(1);
        assertEquals(2, first);
        assertEquals(3, second);
    }

    @Test
    public void testSkippingExtraArguments() {
        final var argParser = new ArgumentParser("testParser", true);
        argParser.argument(
                ArgType.Integer, "addedNumbers", "Added numbers"
        ).multiple(2);
        final var output = argParser.argument(
                ArgType.String, "output", "Output file"
        );
        argParser.option(
                ArgType.Boolean, "debug", "d", "Debug mode"
        );
        argParser.parse("2", "-d", "3", "out.txt", "something", "else", "in", "string");
        assertEquals("out.txt", output.getValue());
    }
}