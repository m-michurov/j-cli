package ru.nsu.g.mmichurov.jcli.outer;

import org.jetbrains.annotations.NotNull;

public enum DataSourceEnum {
    LOCAL,
    STAGING,
    PRODUCTION;

    @Override
    public @NotNull String toString() {
        return switch (this) {
            case LOCAL -> "local";
            case STAGING -> "staging";
            case PRODUCTION -> "production";
        };
    }
}
