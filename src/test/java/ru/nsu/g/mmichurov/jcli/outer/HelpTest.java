package ru.nsu.g.mmichurov.jcli.outer;

import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import ru.nsu.g.mmichurov.jcli.*;

import java.util.Collections;
import java.util.EnumSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelpTest {

    @Test
    public void testHelpMessage() {
        final var argParser = new ArgumentParser("test");
        argParser.argument(
                ArgType.String, "mainReport", "Main report for analysis"
        );
        argParser.argument(
                ArgType.String, "compareToReport", "Report to compare to"
        ).optional();

        argParser.option(
                ArgType.String, "output", "o", "Output file"
        );
        argParser.option(
                ArgType.Double, "eps", "e", "Meaningful performance changes"
        ).setDefault(1.0);

        argParser.option(
                ArgType.Boolean, "short", "s", "Show short version of report"
        ).setDefault(false);

        argParser.option(
                ArgType.choiceOf(Stream.of("text", "html", "teamcity", "statistics", "metrics").sorted().collect(
                        Collectors.toList())), "renders",
                "r", "Renders for showing information"
        ).multiple().setDefault(Collections.singletonList("text"));

        argParser.option(
                ArgType.choiceOf(DataSourceEnum.class), "sources", "ds", "Data sources"
        ).multiple().setDefault(Collections.singletonList(DataSourceEnum.PRODUCTION));

        argParser.option(
                ArgType.String, "user", "u", "User access information for authorization"
        );

        argParser.parse("main.txt");
        final var helpOutput = argParser.makeUsage();
        final var expectedOutput = String.join("\n", new String[]{
                "Usage: test options_list",
                "Arguments:",
                "    mainReport -> Main report for analysis { String }",
                "    compareToReport -> Report to compare to (optional) { String }",
                "Options:",
                "    --output, -o -> Output file { String }",
                "    --eps, -e [1.0] -> Meaningful performance changes { Double }",
                "    --short, -s [false] -> Show short version of report ",
                String.format(
                        "    --renders, -r [text] -> Renders for showing information { Value should be one of %s }",
                        Stream.of("text", "html", "teamcity", "statistics", "metrics").sorted().collect(
                                Collectors.toList())
                ),
                String.format(
                        "    --sources, -ds [production] -> Data sources { Value should be one of %s }",
                        EnumSet.allOf(DataSourceEnum.class).stream()
                                .map(it -> it.toString().toLowerCase()).sorted().collect(Collectors.toList())
                ),
                "    --user, -u -> User access information for authorization { String }",
                "    --help, -h -> Usage info \n"});
        assertEquals(expectedOutput, helpOutput);
    }

    enum MetricType {
        SAMPLES,
        GEOMEAN;

        @Override
        public @NotNull String toString() {
            return switch (this) {
                case GEOMEAN -> "geomean";
                case SAMPLES -> "samples";
            };
        }
    }

    @Test
    public void testHelpForSubcommands() {
        class Summary extends Subcommand {

            Summary() {
                super("summary", "Get summary information");

                this.option(
                        ArgType.choiceOf(MetricType.class), "exec", null, "Execution time way of calculation"
                ).setDefault(MetricType.GEOMEAN);
                this.option(
                        ArgType.String, "exec-samples", null,
                        "Samples used for execution time metric (value 'all' allows use all samples)"
                ).delimiter(",");
                this.option(
                        ArgType.String, "exec-normalize", null,
                        "File with golden results which should be used for normalization"
                );
                this.option(ArgType.choiceOf(MetricType.class), "compile", null,
                        "Compile time way of calculation"
                ).setDefault(MetricType.GEOMEAN);
                this.option(
                        ArgType.String, "compile-samples", null,
                        "Samples used for compile time metric (value 'all' allows use all samples)"
                ).delimiter(",");
                this.option(
                        ArgType.String, "compile-normalize", null,
                        "File with golden results which should be used for normalization"
                );
                this.option(
                        ArgType.choiceOf(MetricType.class), "codesize", null,
                        "Code size way of calculation"
                ).setDefault(MetricType.GEOMEAN);
                this.option(
                        ArgType.String, "codesize-samples", null,
                        "Samples used for code size metric (value 'all' allows use all samples)"
                ).delimiter(",");
                this.option(ArgType.String, "codesize-normalize", null,
                        "File with golden results which should be used for normalization"
                );
                this.option(
                        ArgType.choiceOf(DataSourceEnum.class), "source", null,
                        "Data source").setDefault(DataSourceEnum.PRODUCTION
                );
                this.option(
                        ArgType.String, "source-samples", null,
                        "Samples used for code size metric (value 'all' allows use all samples)"
                ).delimiter(",");
                this.option(
                        ArgType.String, "source-normalize", null,
                        "File with golden results which should be used for normalization"
                );
                this.option(
                        ArgType.String, "user", "u",
                        "User access information for authorization"
                );
                this.argument(
                        ArgType.String, "mainReport", "Main report for analysis"
                );
            }
            @Override
            public void execute() {
                System.out.println("Do some important things!");
            }
        }

        final var action = new Summary();
        // Parse args.
        final var argParser = new ArgumentParser("test");
        argParser.subcommands(action);
        argParser.parse("summary", "out.txt");
        final var helpOutput = action.makeUsage();
        final var metricsEnumString = EnumSet.allOf(MetricType.class).stream()
                .map(it -> it.toString().toLowerCase())
                .sorted()
                .collect(Collectors.toList());
        final var sourcesEnumString = EnumSet.allOf(DataSourceEnum.class).stream()
                .map(it -> it.toString().toLowerCase())
                .sorted()
                .collect(Collectors.toList());
        final var expectedOutput = String.format(String.join("\n", new String[]{
                "Usage: test summary options_list",
                "Arguments:",
                "    mainReport -> Main report for analysis { String }",
                "Options:",
                "    --exec [geomean] -> Execution time way of calculation { Value should be one of %s }",
                "    --exec-samples -> Samples used for execution time metric (value 'all' allows use all samples) { String }",
                "    --exec-normalize -> File with golden results which should be used for normalization { String }",
                "    --compile [geomean] -> Compile time way of calculation { Value should be one of %s }",
                "    --compile-samples -> Samples used for compile time metric (value 'all' allows use all samples) { String }",
                "    --compile-normalize -> File with golden results which should be used for normalization { String }",
                "    --codesize [geomean] -> Code size way of calculation { Value should be one of %s }",
                "    --codesize-samples -> Samples used for code size metric (value 'all' allows use all samples) { String }",
                "    --codesize-normalize -> File with golden results which should be used for normalization { String }",
                "    --source [production] -> Data source { Value should be one of %s }",
                "    --source-samples -> Samples used for code size metric (value 'all' allows use all samples) { String }",
                "    --source-normalize -> File with golden results which should be used for normalization { String }",
                "    --user, -u -> User access information for authorization { String }",
                "    --help, -h -> Usage info \n"
        }), metricsEnumString, metricsEnumString, metricsEnumString, sourcesEnumString);
        assertEquals(expectedOutput, helpOutput);
    }

    @Test
    public void testHelpMessageWithSubcommands() {
        abstract class CommonOptions extends Subcommand {
            protected final Arguments.MultipleRequiredArgument<Integer> numbers =
                    this.argument(ArgType.Integer, "numbers", "Numbers").vararg();

            CommonOptions(final @NotNull String name, final @NotNull String actionDescription) {
                super(name, actionDescription);
            }
        }

        class Summary extends CommonOptions {
            private final Options.SingleOptionWithDefault<Boolean> invert =
                    this.flag("invert", "i", "Invert results");
            int result = 0;

            Summary() {
                super("summary", "Calculate summary");
            }

            @Override
            public void execute() {
                this.result = this.numbers.getValue().stream().reduce(0, Integer::sum);
                this.result = this.invert.getValue() ?  -1 * this.result : this.result;
            }
        }

        class Subtraction extends CommonOptions {
            int result = 0;

            Subtraction() {
                super("sub", "Calculate subtraction");
            }

            @Override
            public void execute() {
                this.result = this.numbers.getValue().stream().reduce(0, (x, y) -> (-x - y));
            }
        }

        final var summaryAction = new Summary();
        final var subtractionAction = new Subtraction();
        final var argParser = new ArgumentParser("testParser");
        argParser.subcommands(summaryAction, subtractionAction);
        argParser.parse();
        final var helpOutput = argParser.makeUsage();
        System.out.println(helpOutput);
        final var expectedOutput = String.join("\n", new String[]{
                "Usage: testParser options_list",
                "Subcommands:",
                "    summary - Calculate summary",
                "    sub - Calculate subtraction",
                "Options:",
                "    --help, -h -> Usage info \n"
        });
        assertEquals(expectedOutput, helpOutput);
    }
}
