package ru.nsu.g.mmichurov.jcli.outer;

import ru.nsu.g.mmichurov.jcli.ArgType;
import ru.nsu.g.mmichurov.jcli.ArgumentParser;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

public class ArgTypeTest {

    @Test
    public void testCreateType() {
        final var fileType = new ArgType<@NotNull File>(true) {

            @Override
            public @NotNull java.lang.String getDescription() {
                return "{ File path }";
            }

            @Override
            public @NotNull File convert(
                    final @NotNull java.lang.String value,
                    final @NotNull java.lang.String optionName) {
                return new File(value);
            }
        };

        final var argParser = new ArgumentParser("testParser");
        final var file = argParser.argument(
                fileType, "file", "Input file path"
        );
        final var helpMessage = argParser.makeUsage();
        assertTrue(helpMessage.contains("    file -> Input file path { File path }\n"));
        argParser.parse("./build.gradle");
        Assertions.assertEquals(ArgumentParser.ValueOrigin.SET_BY_USER, file.getValueOrigin());
        System.out.println(file.getValue().exists()); // true
    }
}
